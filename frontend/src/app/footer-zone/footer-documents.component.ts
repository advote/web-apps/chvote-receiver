/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { CurrentVotingService } from '../operation/current-voting-service';
import { TranslateService } from '@ngx-translate/core';
import { Documentation } from '../model/documentation';
import { HttpParameters } from '../core/http/http.parameters.service';
import { Subscription } from 'rxjs';
import { DocumentationService } from '../core/service/documentation.service';

@Component({
  selector: 'footer-documents',
  templateUrl: './footer-documents.component.html',
  styleUrls: ['./footer-documents.component.scss']
})
export class FooterDocumentsComponent implements OnInit, OnDestroy {
  documents: Documentation[];
  private subscriptions: Subscription[] = [];

  constructor(private httpParams: HttpParameters,
              private currentVotingService: CurrentVotingService,
              private translateService: TranslateService,
              private documentationService: DocumentationService) {
    this.subscriptions.push(
      translateService.onLangChange.subscribe(() => {
        this.updateDocuments();
      })
    );
  }

  ngOnInit() {
    this.updateDocuments();
  }

  getAccessibilityProperty(documentation: Documentation): string {
    return 'footer.' + documentation.type.toLowerCase() + '.accessibility';
  }

  getLabelProperty(documentation: Documentation): string {
    return 'footer.' + documentation.type.toLowerCase() + '.label';
  }

  getUrl(documentation: Documentation): string {
    return this.documentationService.getDocumentationUrl(documentation.fileIndex);
  }

  private updateDocuments() {
    this.documents = [];
    this.addDocument('FAQ');
    this.addDocument('TERMS');
  }

  private addDocument(type: string) {
    const doc = this.getByBestMatchingLang(this.getByType(type));
    if (doc !== undefined) {
      this.documents.push(doc);
    }
  }

  private getByType(type: string) {
    return this.currentVotingService.operation.documentations
      .filter(doc => doc.type === type);
  }

  private getByBestMatchingLang(documentations: Documentation[]) {
    const doc = this.filterByLang(documentations, this.translateService.currentLang);
    return doc !== undefined ? doc :
      this.filterByLang(documentations, this.currentVotingService.operation.defaultLang.toLowerCase());
  }

  private filterByLang(documentations: Documentation[], lang: string) {
    return documentations.find(doc => doc.lang.toLowerCase() === lang);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
