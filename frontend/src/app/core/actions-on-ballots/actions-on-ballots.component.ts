/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input } from "@angular/core";
import { CurrentVotingService } from '../../operation/current-voting-service';
import { Votation } from '../../model/votation';
import { ElectionBallot } from '../../model/election';
import { ActivatedRoute, Router } from '@angular/router';
import { ChvoteDialogComponent } from '../chvote-dialog/chvote-dialog.component';
import { MatDialog } from '@angular/material';
import { LocalizationService } from '../service/localization.service';

@Component({
  selector: 'actions-on-ballots',
  templateUrl: './actions-on-ballots.component.html',
  styleUrls: ['./actions-on-ballots.component.scss']
})

export class ActionsOnBallotsComponent {
  @Input() ballots: (Votation | ElectionBallot)[];
  @Input() isVotation: boolean;
  @Input() disabled: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private currentVotingService: CurrentVotingService,
              private localizationService: LocalizationService,
              private dialog: MatDialog) {
  }

  get displayCancel(): boolean {
    return !this.currentVotingService.hasSingleBallot();
  }

  modify(): void {
    if (this.isVotation) {
      CurrentVotingService.copyVotationBallotInBuffer(this.ballots as Votation[]);
      if (this.ballots.length > 1) {
        // navigate on general page
        this.router.navigate(['../votationBallot'], {relativeTo: this.route});
      } else {
        // navigate to a single ballot
        this.router.navigate(['../votationBallot/'],
          {relativeTo: this.route, queryParams: {domainOfInfluence: this.ballots[0].domainOfInfluence}});
      }
    } else {
      // TODO: navigate to election ballot page
    }
  }

  cancel(): void {
    if (this.currentVotingService.participationCount() === 1) {
      // cancelling the last ballot is forbidden
      this.dialog.open(ChvoteDialogComponent, {
        width: '500px',
        disableClose: true,
        data: {
          messageKey: 'ballot-list.dialog.cancel-forbidden',
          primaryButtonKey: 'global.actions.close'
        }
      });

    } else {
      // display confirmation message
      this.dialog.open(ChvoteDialogComponent, {
        width: '500px',
        disableClose: true,
        data: {
          messageKey: 'ballot-list.dialog.confirm-cancel-participation',
          messageParams: {
            ballot: this.localizationService.translate(this.ballots[0].localizedLabel)
          },
          primaryButtonKey: 'global.actions.yes',
          secondaryButtonKey: 'global.actions.no'
        }
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {
          this.ballots.forEach(ballot => this.currentVotingService.removeVoteFor(ballot));
        }
      });
    }
  }

}
