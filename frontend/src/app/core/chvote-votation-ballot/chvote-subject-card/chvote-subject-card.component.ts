/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Answer, Subject } from '../../../model/votation';
import { CurrentVotingService } from '../../../operation/current-voting-service';

@Component({
  selector: '[chvote-subject-card]',
  templateUrl: './chvote-subject-card.component.html',
  styleUrls: ['./chvote-subject-card.component.scss']
})
export class ChvoteSubjectCardComponent implements OnInit {

  @Input()
  display: 'ballot' | 'summary' | 'control';

  @Input()
  subject: Subject;

  @Input()
  displaySeparator: boolean;

  @Input()
  selectedAnswer: Answer;

  @Output() selectedAnswerChange = new EventEmitter();

  virtualAnswer: Answer;

  constructor(private currentVotingService: CurrentVotingService) {
  }

  ngOnInit(): void {
    this.virtualAnswer = this.subject.answers.find(answer => answer.virtual);
  }

  toggle(answer: Answer): void {
    if (answer === this.selectedAnswer) {
      this.selectedAnswer = this.virtualAnswer;
    } else {
      this.selectedAnswer = answer;
    }
    this.selectedAnswerChange.emit(this.selectedAnswer);
  }

  get displayableAnswers(): Answer[] {
    return this.subject.answers.filter(answer => !answer.virtual)
  }

  get index(): string {
    return this.currentVotingService.computeSubjectIndex(this.subject);
  }
}
