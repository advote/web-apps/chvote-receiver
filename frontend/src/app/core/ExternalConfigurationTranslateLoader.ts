/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { TranslateLoader } from '@ngx-translate/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { deepMergeTranslations, i18n } from '../i18n/i18n';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { AssetService } from './service/asset.service';
import { Operation } from '../model/operation';

/**
 * This class retrieve translations for the vote receiver
 * It supports both static translations and operation related translations
 * Operation related translations always take precedence
 */
@Injectable()
export class ExternalConfigurationTranslateLoader extends TranslateLoader {
  private translations = new BehaviorSubject(i18n);
  private lastLoadedProtocolInstanceId: string;

  constructor(private assetService: AssetService) {
    super();
  }

  /**
   * Ask a load of translation for a given operation
   * One should call method TranslateService.reloadLang for change to take effect
   * @param {Operation} protocolInstanceId the protocol instance identifier to get translation from
   * @return {Observable<any>} emit an event if the translation is ready
   */
  loadForOperation(protocolInstanceId: string): Observable<any> {
    if (protocolInstanceId === this.lastLoadedProtocolInstanceId) {
      return of(true);
    }
    return this.assetService.findTranslations(protocolInstanceId)
      .pipe(
        tap((result) => {
          this.translations.next(deepMergeTranslations(i18n, result));
          this.lastLoadedProtocolInstanceId = protocolInstanceId;
        }));
  }

  getTranslation(lang: string): Observable<any> {
    return this.translations.pipe(
      map(v => v[lang])
    )
  }

}
