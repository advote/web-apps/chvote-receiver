/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Injectable } from '@angular/core';
import { BaseUrlService } from '../base-url/base-url.service';
import { CurrentVotingService } from '../../operation/current-voting-service';

@Injectable()
export class DocumentationService {
  private readonly serviceUrl: string;

  constructor(private baseUrlService: BaseUrlService,
              private currentVotingService: CurrentVotingService) {
    this.serviceUrl = `${baseUrlService.getBackendBaseUrl()}`;
  }

  /**
   * Compute the URL to access a given documentation
   *
   * @param documentationIndex the documentation's unique index
   * @return the correctly formed URL to access the documentation
   */
  getDocumentationUrl(documentationIndex: number) {
    return `${this.serviceUrl}/asset/document?protocolInstanceId=${this.currentVotingService.operation.protocolInstanceId}&fileIndex=${documentationIndex}`;
  }
}
