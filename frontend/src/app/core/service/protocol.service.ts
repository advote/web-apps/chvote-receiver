/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/


import { first, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {
  BallotQueryAndRand, BigInteger, Confirmation, EncryptionPublicKey, EncryptionPublicKeyJSON, FinalizationCodePart,
  FinalizationCodePartJSON, GeneralAlgorithms, Hash, KeyEstablishment, ObliviousTransferResponse,
  ObliviousTransferResponseJSON, Point, PrimesCache, PublicParameters, PublicParametersJSON, VoteCasting,
  VoteConfirmation
} from 'chvote-protocol-client';
import { HttpParameters } from '../http/http.parameters.service';
import { Blake2bService } from '@protocoder/blake2';
import 'rxjs/Rx';
import * as _ from 'underscore';
import { CurrentVotingService } from '../../operation/current-voting-service';

@Injectable()
export class ProtocolService {
  private serviceUrl: string;
  private voteCasting: VoteCasting;
  private voteConfirmation: VoteConfirmation;
  private publicKeyParts: EncryptionPublicKey[];

  private _protocolId: string;
  private _identificationCredentials: string;
  private _voterId: string;
  private _finalizationCode: string;

  private selections: number[];
  private randomizations: BigInteger[];
  private pointMatrix: Point[][];
  private obliviousTransferResponses: ObliviousTransferResponse[];

  constructor(private http: HttpClient,
              private httpParameters: HttpParameters,
              private blake2bService: Blake2bService,
              private currentVotingService: CurrentVotingService) {
    this.serviceUrl = `${this.httpParameters.apiBaseURL}/protocol`;
  }

  set identificationCredentials(value: string) {
    this._identificationCredentials = value;
  }

  set voterId(value: string) {
    this._voterId = value;
  }

  get protocolId(): string {
    return this._protocolId;
  }

  get voterId(): string {
    return this._voterId;
  }

  public initialize(protocolId: string): void {
    this._protocolId = protocolId;
    forkJoin(
      this.getPublicParameters(this._protocolId),
      this.getPublicKeys(this._protocolId),
      this.getPrimes(this._protocolId)
    ).subscribe(results => {
      let publicParameters: PublicParameters = results[0] as PublicParameters;
      this.publicKeyParts = results[1];

      let hash: Hash = new Hash(this.blake2bService, publicParameters.securityParameters.upper_l);
      let primesCache: PrimesCache = new PrimesCache(results[2] as BigInteger[], publicParameters.encryptionGroup);
      let generalAlgo: GeneralAlgorithms = new GeneralAlgorithms(hash, publicParameters.encryptionGroup,
        publicParameters.identificationGroup, primesCache);

      this.voteCasting = new VoteCasting(publicParameters, generalAlgo, hash);
      this.voteConfirmation = new VoteConfirmation(publicParameters, generalAlgo);
    });
  }

  public submitBallot(birthDate): Observable<void> {
    // 1. compute voter's selection
    this.selections = this.currentVotingService.buildSelection();
    // 2. build system public key
    let systemPublicKey: EncryptionPublicKey = KeyEstablishment.getPublicKey(this.publicKeyParts);
    // 3. encrypt ballot
    let ballotQueryAndRand: BallotQueryAndRand =
      this.voteCasting.genBallot(this._identificationCredentials, this.selections, systemPublicKey);
    // 4. keep randomization for verification codes
    this.randomizations = ballotQueryAndRand.bold_r;
    // 5. send encrypted ballot to server and keep oblivious transfer response
    return this.http.post<ObliviousTransferResponseJSON[]>(
      `${this.serviceUrl}/submit-ballot`,
      {
        birthDate,
        ballotAndQuery: ballotQueryAndRand.alpha.toJSON()
      }, {params: {protocolInstanceId: this._protocolId, voterId: this._voterId}}).pipe(
      map(responsesJSON => {
        this.obliviousTransferResponses = [];
        for (let response of responsesJSON) {
          this.obliviousTransferResponses.push(ObliviousTransferResponse.fromJSON(response));
        }
      }));
  }

  public retrieveVerificationCodes(): string[] {
    try {
      this.pointMatrix = this.voteCasting.getPointMatrix(
        this.obliviousTransferResponses,
        this.selections,
        this.randomizations
      );
    }
    catch (error) {
      throw new Error(`Error while computing the point matrix: ${error.message}`);
    }
    return this.voteCasting.getReturnCodes(this.selections, this.pointMatrix);
  }

  public submitConfirmationCode(confirmationCredentials: string): Observable<void> {
    // 1. generate confirmation payload
    let confirmation: Confirmation = this.voteConfirmation.genConfirmation(confirmationCredentials, this.pointMatrix);
    // 2. send confirmation to server
    return this.http.post<FinalizationCodePartJSON[]>(
      `${this.serviceUrl}/submit-confirmation-code`,
      confirmation.toJSON(), {params: {protocolInstanceId: this._protocolId, voterId: this._voterId}}
    ).pipe(map(responsesJSON => {
      // 3. retrieve finalization code
      let finalizationCodeParts = [];
      for (let response of responsesJSON) {
        finalizationCodeParts.push(FinalizationCodePart.fromJSON(response));
      }
      this._finalizationCode = this.voteConfirmation.getFinalizationCode(finalizationCodeParts);
    }));
  }

  public get finalizationCode(): string {
    return this._finalizationCode;
  }

  private getPublicParameters(protocolId: string): Observable<PublicParameters> {
    return this.http.get<PublicParametersJSON>(`${this.serviceUrl}/public-parameters/`,
      {params: {protocolInstanceId: protocolId}}).pipe(first(),
      map(json => PublicParameters.fromJSON(json)),);
  }

  private getPrimes(protocolId: string): Observable<BigInteger[]> {
    return this.http.get<string[]>(`${this.serviceUrl}/primes`, {params: {protocolInstanceId: protocolId}})
      .pipe(first(),
        map(primes => _.map(primes, function (prime: string) {
          return new BigInteger(prime);
        })),);
  }

  private getPublicKeys(protocolId: string): Observable<EncryptionPublicKey[]> {
    return this.http.get<EncryptionPublicKeyJSON[]>(`${this.serviceUrl}/public-keys`,
      {params: {protocolInstanceId: protocolId}}).pipe(first(),
      map(keysJSON => {
        let keys: EncryptionPublicKey[] = [];
        for (let key of keysJSON) {
          keys.push(EncryptionPublicKey.fromJSON(key));
        }
        return keys;
      }),);
  }
}
