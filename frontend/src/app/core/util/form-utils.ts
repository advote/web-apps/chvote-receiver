/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

export const REGEX_ALPHA_NUMERICAL_WITH_ACCENT = /^[ \wáàâäãåçéèêëíìîïñóòôöõúùûüýÿæœßÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]*$/;


export function validateRegex(nameRe: RegExp, errorKey: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    if (nameRe.test(control.value)) {
      return null;
    }
    let error = {};
    error[errorKey] = {value: control.value};
    return error;
  };
}

export function focusOnError(form: FormGroup) {
  let invalidField = Object.keys(form.controls)
    .find(k => !form.controls[k].valid && form.controls[k].enabled);
  form.controls[invalidField].markAsDirty();
  let elementToFocus = <any>document.querySelector(`mat-select[formcontrolname='${invalidField}']`);
  if (elementToFocus && elementToFocus.click) {
    elementToFocus.click();
  } else {
    elementToFocus = <any>document.querySelector(`*[formcontrolname='${invalidField}']`);
    if (elementToFocus && elementToFocus.focus) {
      elementToFocus.focus();
    }
  }
}

export function enableFormComponents(formComponents: any, enable: boolean) {
  Object.keys(formComponents).forEach(key => enableFormComponent(formComponents[key], enable));
}


export function enableFormComponent(formComponent: any, enable: boolean) {

  if (enable && formComponent.disabled) {
    formComponent.enable();
  } else if (!enable && formComponent.enabled) {
    formComponent.disable();
  }
}

export function isFormModifiedAndNotSaved(form: any, hasBeenSaved: boolean) {
  return !hasBeenSaved && !!Object.keys(form.controls).find(k => !form.controls[k].pristine)
}

export const labelNotAlreadyUsed = function(labelsProvider : () => string[]) : ValidatorFn  {
  return c => {
    if (labelsProvider().indexOf(c.value) >= 0) {
      return {"label-already-used": true};
    } else {
      return null;
    }
  }
};
