/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, Input, OnInit } from '@angular/core';
import { Step, StepStatus } from './vote-workflow-view.component';

@Component({
  selector: 'li[vote-workflow-step]',
  templateUrl: './vote-workflow-step.component.html',
  styleUrls: ['./vote-workflow-view.component.scss']
})
export class VoteWorkflowStepComponent implements OnInit {

  @Input()
  stepStatus: StepStatus;

  @Input()
  stepNumber: number;

  @Input()
  step: Step;

  ngOnInit() {
  }

  isStatusDone(): boolean {
    return this.stepStatus === StepStatus.done;
  }
}
