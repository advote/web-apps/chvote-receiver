/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export enum Step {
  Identification = "identification",
  LegalInformation = "legal-information",
  BallotList = 'ballot-list',
  Summary = 'summary',
  Control = 'control',
  Finalization = 'finalization'
}

export enum StepStatus {
  done = "done",
  current = "current",
  todo = "todo"
}

@Component({
  selector: 'vote-workflow-view',
  templateUrl: './vote-workflow-view.component.html',
  styleUrls: ['./vote-workflow-view.component.scss']
})
export class VoteWorkflowViewComponent implements OnInit {

  allSteps = [
    {stepNumber: 1, step: Step.Identification},
    {stepNumber: 2, step: Step.LegalInformation},
    {stepNumber: 3, step: Step.BallotList},
    {stepNumber: 4, step: Step.Summary},
    {stepNumber: 5, step: Step.Control},
    {stepNumber: 6, step: Step.Finalization}
  ];

  currentStep: Step;

  getStepState(step): StepStatus {
    let currentStepIndex = Object.keys(Step).map(i => Step[i]).indexOf(this.currentStep);
    let stepIndex = Object.keys(Step).map(i => Step[i]).indexOf(step);

    if (currentStepIndex === 5) {
      return StepStatus.done
    }

    if (currentStepIndex === stepIndex) {
      return StepStatus.current
    }

    if (stepIndex < currentStepIndex) {
      return StepStatus.done
    }

    return StepStatus.todo

  }

  getStepNumber(step) {
    return this.allSteps.filter(item => item.step === step)[0];
  }

  constructor(route: ActivatedRoute) {
    route.data.subscribe(data => {
      this.currentStep = data.step;
    })
  }

  ngOnInit() {
  }
}
