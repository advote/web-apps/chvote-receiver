/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Step, VoteWorkflowViewComponent } from '../vote-workflow-display/vote-workflow-view.component';
import { CurrentVotingService } from '../current-voting-service';
import { Subscription } from 'rxjs';
import { ElectionBallot } from '../../model/election';
import { Votation } from '../../model/votation';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ChvoteDialogComponent } from '../../core/chvote-dialog/chvote-dialog.component';

@Component({
  selector: 'app-ballots-list',
  templateUrl: './ballots-list.component.html',
  styleUrls: ['./ballots-list.component.scss']
})

export class BallotsListComponent implements OnInit, OnDestroy {
  currentStepNumber;
  nextStepNumber;
  private changeSubscription: Subscription;
  private filledBallotsCounter: number;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private currentVotingService: CurrentVotingService,
              private dialog: MatDialog) {
  }

  get votations(): Votation[] {
    return this.currentVotingService.votations;
  }

  get elections(): ElectionBallot[] {
    return this.currentVotingService.elections;
  }

  get groupVotation(): boolean {
    return this.currentVotingService.operation.groupVotation;
  }

  ngOnInit() {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.BallotList);
    this.nextStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Summary);

    this.changeSubscription = this.currentVotingService.currentVotingChange.subscribe(() => this.update());
    this.update()
    this.currentVotingService.lastVisitedPage = 'BALLOT_SELECTION';
  }

  ngOnDestroy(): void {
    this.changeSubscription.unsubscribe();
  }

  quit(): void {
  }

  continue(): void {
    if (this.currentVotingService.participationCount() === 0) {
      // at list one ballot should be filled
      this.dialog.open(ChvoteDialogComponent, {
        width: '500px',
        disableClose: true,
        data: {
          messageKey: 'ballot-list.dialog.participation-required',
          primaryButtonKey: 'global.actions.back'
        }
      });

    } else if (this.currentVotingService.participationCount() < this.votations.length + this.elections.length) {
      // some ballots are not filled
      // display confirmation message
      this.dialog.open(ChvoteDialogComponent, {
        width: '500px',
        disableClose: true,
        data: {
          messageKey: 'ballot-list.dialog.confirm-participation',
          primaryButtonKey: 'ballot-list.dialog.confirm-participation-continue',
          secondaryButtonKey: 'global.actions.back'
        }
      }).afterClosed().subscribe((accept: boolean) => {
        if (accept) {
          this.router.navigate(['../summary'], {relativeTo: this.route});
        }
      });

    } else {
      this.router.navigate(['../summary'], {relativeTo: this.route});
    }
  }

  get counts() {
    return {
      totalBallotsCounter: this.totalBallotsCounter,
      filledBallotsCounter: this.filledBallotsCounter
    }
  }

  private get totalBallotsCounter() {
    let votationCount = 0;
    if (this.votations.length > 0) {
      votationCount = (this.groupVotation) ? 1 : this.votations.length;
    }

    return votationCount + this.elections.length;
  }

  private update() {
    let votationCount = this.countVoted(this.currentVotingService.votations);

    if (votationCount > 0 && this.groupVotation) {
      votationCount = 1;
    }

    let electionCount = this.countVoted(this.currentVotingService.elections);

    this.filledBallotsCounter = votationCount + electionCount;
  }

  private countVoted<Ballot extends ElectionBallot | Votation>(ballots: Ballot[]) {
    return ballots.map(ballot =>
      this.currentVotingService.hasVotedFor(ballot)
    ).reduce((counter, hasVoted) => counter + (hasVoted ? 1 : 0), 0)

  }
}
