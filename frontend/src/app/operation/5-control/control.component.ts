/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Step, VoteWorkflowViewComponent } from "../vote-workflow-display/vote-workflow-view.component";
import { ActivatedRoute, Router } from "@angular/router";
import { ProtocolService } from '../../core/service/protocol.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { focusOnError } from '../../core/util/form-utils';
import { Votation } from '../../model/votation';
import { ElectionBallot } from '../../model/election';
import { CurrentVotingService } from '../current-voting-service';
import { FinalizationService } from '../../core/service/finalization.service';
import { ChvoteDialogComponent } from '../../core/chvote-dialog/chvote-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'control, [control]',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})

export class ControlComponent implements OnInit {
  currentStepNumber;
  nextStepNumber;
  controlForm: FormGroup;
  busy: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private voteWorkflowViewComponent: VoteWorkflowViewComponent,
              private currentVotingService: CurrentVotingService,
              private protocolService: ProtocolService,
              private dialog: MatDialog,
              private finalizationService: FinalizationService) {
    this.createForm();
  }

  ngOnInit(): void {
    this.currentStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Control);
    this.nextStepNumber = this.voteWorkflowViewComponent.getStepNumber(Step.Finalization);

    // retrieve verification codes
    let verificationCodes: string[] = this.protocolService.retrieveVerificationCodes();

    // populate selected answers with verification codes
    this.currentVotingService.populateVerificationCodes(verificationCodes);

    this.currentVotingService.lastVisitedPage = 'CONTROL';
  }

  get votations(): Votation[] {
    return this.currentVotingService.votations;
  }

  get elections(): ElectionBallot[] {
    return this.currentVotingService.elections;
  }

  quit() {
    // TODO
  }

  vote() {

    this.controlForm.controls.confirmationCode.markAsTouched();
    this.controlForm.updateValueAndValidity();
    if (this.controlForm.valid) {
      this.busy = true;
      this.protocolService.submitConfirmationCode(this.controlForm.getRawValue().confirmationCode).subscribe(() => {
          // log finalization code
          this.finalizationService.logFinalization(
            this.protocolService.protocolId,
            this.protocolService.voterId,
            this.protocolService.finalizationCode
          ).subscribe(() => {
            this.router.navigate(['../finalization'], {relativeTo: this.route});
            this.busy = false;
          });
        },
        errorProvider => {
          this.busy = false;
          errorProvider.onValidationError(/.*ConfirmationCheckException/, error => this.displayErrorDialog(error))
        });
    } else {
      focusOnError(this.controlForm)
    }
  }


  private displayErrorDialog(error) {
    let canRetry = error.globalErrors[0].messageParams[1] == "true";
    let data: any = {
      type: canRetry ? 'warning' : 'error',
      titleKey: 'control.confirmation-code.error-dialog.title',
      messageKey: `control.confirmation-code.error-dialog.${error.globalErrors[0].messageKey}`,
      messageParams: {
        count: error.globalErrors[0].messageParams[0]
      }
    };
    if (canRetry) {
      data.primaryButtonKey = 'global.actions.retry'
    } else {
      data.secondaryButtonKey = 'global.actions.quit'
    }

    this.dialog.open(ChvoteDialogComponent, {
      width: '500px',
      disableClose: true,
      data: data
    }).afterClosed().subscribe(() => {
      if (!canRetry) {
        this.quit();
      }
    });
  }

  private createForm() {
    this.controlForm = this.formBuilder.group({
      confirmationCode: [null, [Validators.required]]
    });
  }
}
