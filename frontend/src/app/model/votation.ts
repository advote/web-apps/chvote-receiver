/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Ballot } from './ballot';

export interface Answer {
  protocolIndex: number;
  virtual: boolean;
  localizedLabel: {
    [key: string]: string
  };

  // Verification code linked to this answer,
  // This information is mutable in the frontend
  verificationCode: string;
}

export interface Subject {
  number: string;
  localizedLabel: {
    [key: string]: string
  };
  answers: Answer[];

  // answer currently selected in the ballot page, before validation
  // This information is mutable in the frontend
  bufferedAnswer: Answer;

  // Currently selected answer (after validation),
  // This information is mutable in the frontend
  selectedAnswer: Answer;
}

export interface VariantSubjects {
  localizedGroupLabel: {
    [key: string]: string
  };
  subjects: Subject[];
  tieBreak: Subject;
}

export interface Votation extends Ballot {
  subjects: Subject[];
  variantSubjects: VariantSubjects[];
}

