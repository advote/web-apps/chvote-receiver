/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a button.
 */
export class Button {

  private button: ElementFinder;

  /**
   * Create a button based on its unique ID or the element directly.
   *
   * @param idOrElement the button's ID or the button element directly
   * @param b the browser on which this button appears (main browser by default)
   */
  constructor(idOrElement: string | ElementFinder,
              private b: ProtractorBrowser = browser) {
    this.button = (typeof idOrElement === "string") ? b.element(by.id(idOrElement)) : idOrElement;
  }

  /**
   * @returns the text of the button
   */
  get text(): promise.Promise<string> {
    return this.button.element(by.tagName('span')).getText();
  }

  /**
   * @returns true if the button is present in the DOM, false otherwise
   */
  get present(): promise.Promise<boolean> {
    return this.button.isPresent();
  }

  /**
   * @returns true if the button is displayed, false otherwise
   */
  get displayed(): promise.Promise<boolean> {
    return this.button.isPresent();
  }

  /**
   * @returns true if the button is enabled, false otherwise
   */
  get enabled(): promise.Promise<boolean> {
    return this.button.isEnabled();
  }

  /**
   * Click on the button
   */
  click(): void {
    this.button.click();
    this.b.waitForAngular();
  }
}
