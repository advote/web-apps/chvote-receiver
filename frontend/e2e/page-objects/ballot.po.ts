/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Button } from '../shared/button';
import { ChvoteCheckbox } from '../shared/chvote-checkbox';

/**
 * Page object for the ballot page.
 */
export class BallotPage {

  /**
   * @returns the button to erase all choices
   */
  static get eraseButton(): Button {
    return new Button('erase-button');
  }

  /**
   * @returns the button to go back to ballot's selection page
   */
  static get backButton(): Button {
    return new Button('back-button');
  }

  /**
   * Get a reference to the given CHVote checkbox
   *
   * @param index checkbox's index
   * @return the corresponding ChvoteCheckbox object
   */
  static getCheckbox(index: number): ChvoteCheckbox {
    return new ChvoteCheckbox(`answer-${index}`);
  }
}

