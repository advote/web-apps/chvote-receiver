/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

import { Button } from '../shared/button';
import { by, element, ElementFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

/**
 * Page object for the ballot selection page.
 */
export class BallotSelectionPage {

  /**
   * @return the page's description
   */
  static get description(): promise.Promise<string> {
    return element(by.css('section[role="document"]')).element(by.tagName('strong')).getText();
  }

  /**
   * @return the number of displayed ballots
   */
  static get ballotCount(): promise.Promise<number> {
    return element.all(by.className('ballot-card')).count();
  }

  /**
   * Retrieve a ballot's title
   *
   * @param ballotIndex the index of the ballot
   * @return the ballot's title
   */
  static getBallotTitle(ballotIndex: number): promise.Promise<string> {
    return this.getBallotCard(ballotIndex).element(by.tagName('h4')).getText();
  }

  /**
   * Return number of subject on a ballot
   * @param {number} ballotIndex
   * @return {promise.Promise<string>}
   */
  static getSubjectCount(ballotIndex: number): promise.Promise<string> {
    return this.getBallotCard(ballotIndex).element(by.className('subject-count')).getText();
  }


  /**
   * State a the filled marker is present for a given ballot
   *
   * @param ballotIndex the index of the ballot
   * @return true if the ballot is filled, false otherwise
   */
  static isFilledMarkerPresent(ballotIndex: number): promise.Promise<boolean> {
    return this.getBallotCard(ballotIndex).element(by.className('ballot-filled-marker')).isPresent();
  }

  /**
   * Retrieve a ballot's participate button
   *
   * @param ballotIndex the index of the ballot
   * @returns the button to participate to a given ballot
   */
  static getParticipateButton(ballotIndex: number): Button {
    return new Button(this.getBallotCard(ballotIndex).element(by.className('participate-button')));
  }

  /**
   * Retrieve a ballot's cancel participation button
   *
   * @param ballotIndex the index of the ballot
   * @returns the button to cancel the participation to a given ballot
   */
  static getCancelParticipateButton(ballotIndex: number): Button {
    return new Button(this.getBallotCard(ballotIndex).element(by.className('cancel-button')));
  }

  /**
   * Retrieve a ballot's modify button
   *
   * @param ballotIndex the index of the ballot
   * @returns the button to modify a given ballot
   */
  static getModifyButton(ballotIndex: number): Button {
    return new Button(this.getBallotCard(ballotIndex).element(by.className('modify-button')));
  }

  private static getBallotCard(ballotIndex: number): ElementFinder {
    return element.all(by.className('ballot-card')).get(ballotIndex);
  }
}

