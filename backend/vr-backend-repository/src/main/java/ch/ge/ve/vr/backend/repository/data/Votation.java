/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.data;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Votation ballot data used by the voting site itself.
 */
public class Votation extends Ballot {

  private final List<Subject>         subjects;
  private final List<VariantSubjects> variantSubjects;

  @JsonCreator
  public Votation(@JsonProperty("domainOfInfluence") String domainOfInfluence,
                  @JsonProperty("localizedLabel") Map<Lang, String> localizedLabel,
                  @JsonProperty("documentations") Set<BallotDocumentation> documentations,
                  @JsonProperty("subjects") List<Subject> subjects,
                  @JsonProperty("variantSubjects") List<VariantSubjects> variantSubjects) {
    super(domainOfInfluence, localizedLabel, documentations);
    this.subjects = subjects;
    this.variantSubjects = variantSubjects;
  }

  public List<Subject> getSubjects() {
    return subjects;
  }

  public List<VariantSubjects> getVariantSubjects() {
    return variantSubjects;
  }
}
