/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.entity;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * File representing operation
 */
@Entity
@Table(name = "VR_T_RAW_FILE")
public class RawFile {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "FIL_N_ID", nullable = false)
  private Long id;

  @Column(name = "FIL_N_INDEX", nullable = false)
  private int index;

  @ManyToOne(optional = false)
  @JoinColumn(name = "FIL_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private OperationHolder operation;

  @Lob
  @Column(name = "FIL_X_CONTENT", nullable = false)
  private byte[] content;

  @Column(name = "FIL_C_NAME", nullable = false)
  private String name;

  public Long getId() {
    return id;
  }

  public OperationHolder getOperation() {
    return operation;
  }

  public void setOperation(OperationHolder operation) {
    this.operation = operation;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public byte[] getContent() {
    return content;
  }

  public void setContent(byte[] content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RawFile rawFile = (RawFile) o;
    return index == rawFile.index &&
           id.equals(rawFile.id) &&
           name.equals(rawFile.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, index, name);
  }
}
