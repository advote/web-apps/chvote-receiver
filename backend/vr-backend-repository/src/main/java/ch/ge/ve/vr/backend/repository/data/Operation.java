/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository.data;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Operation's data used by the voting site itself.
 */
public class Operation {
  private final String                    canton;
  private final String                    protocolInstanceId;
  private final OperationType             type;
  private final String                    votingCardLabel;
  private final String                    simulationName;
  private final Lang                      defaultLang;
  private final boolean                   groupVotation;
  private final int                       translationFileIndex;
  private final VotingPeriod              votingPeriod;
  private final LocalDateTime             date;
  private final Set<Documentation>        documentations;
  private final List<HighlightedQuestion> highlightedQuestions;

  @JsonCreator
  public Operation(@JsonProperty("canton") String canton,
                   @JsonProperty("protocolInstanceId") String protocolInstanceId,
                   @JsonProperty("type") OperationType type,
                   @JsonProperty("votingCardLabel") String votingCardLabel,
                   @JsonProperty("simulationName") String simulationName,
                   @JsonProperty("defaultLang") Lang defaultLang,
                   @JsonProperty("groupVotation") boolean groupVotation,
                   @JsonProperty("translationFileIndex") int translationFileIndex,
                   @JsonProperty("votingPeriod") VotingPeriod votingPeriod,
                   @JsonProperty("date") LocalDateTime date,
                   @JsonProperty("documentations") Set<Documentation> documentations,
                   @JsonProperty("highlightedQuestions") List<HighlightedQuestion> highlightedQuestions) {
    this.canton = canton;
    this.protocolInstanceId = protocolInstanceId;
    this.type = type;
    this.votingCardLabel = votingCardLabel;
    this.simulationName = simulationName;
    this.defaultLang = defaultLang;
    this.groupVotation = groupVotation;
    this.translationFileIndex = translationFileIndex;
    this.votingPeriod = votingPeriod;
    this.date = date;
    this.documentations = documentations;
    this.highlightedQuestions = highlightedQuestions;
  }

  public String getCanton() {
    return canton;
  }

  public String getProtocolInstanceId() {
    return protocolInstanceId;
  }

  public OperationType getType() {
    return type;
  }

  public String getVotingCardLabel() {
    return votingCardLabel;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public Lang getDefaultLang() {
    return defaultLang;
  }

  public boolean isGroupVotation() {
    return groupVotation;
  }

  public int getTranslationFileIndex() {
    return translationFileIndex;
  }

  public VotingPeriod getVotingPeriod() {
    return votingPeriod;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public Set<Documentation> getDocumentations() {
    return documentations;
  }

  public List<HighlightedQuestion> getHighlightedQuestions() {
    return highlightedQuestions;
  }
}
