/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.repository;

import ch.ge.ve.vr.backend.repository.entity.ElectionHolder;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository to manipulate coat of arms of each domain of influence, DOI,
 * <p>
 * See : {@link ElectionHolder}
 */
@Repository
public interface ElectionBallotRepository extends JpaRepository<ElectionHolder, Long> {

  /**
   * Get list of elections by operation, protocol instance id and model version
   *
   * @param protocolInstanceId: protocol instance identifier
   * @param modelVersion:       model version
   *
   * @return List of {@link ElectionHolder}
   */
  List<ElectionHolder> findByOperation_ProtocolInstanceIdAndOperation_ModelVersion(
      String protocolInstanceId, Integer modelVersion);

}
