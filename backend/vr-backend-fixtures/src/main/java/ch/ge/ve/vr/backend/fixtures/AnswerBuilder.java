/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.fixtures;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.vr.backend.repository.data.Answer;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Builder for Anwser
 */
public class AnswerBuilder {

  private final AtomicInteger     protocolIndex;
  private       boolean           virtual;
  private       Map<Lang, String> localizedLabel = new EnumMap<>(Lang.class);

  AnswerBuilder(AtomicInteger protocolIndex) {
    this.protocolIndex = protocolIndex;
  }

  public AnswerBuilder virtual(boolean virtual) {
    this.virtual = virtual;
    return this;
  }

  public AnswerBuilder localizedLabel(Lang lang, String label) {
    this.localizedLabel.put(lang, label);
    return this;
  }

  Answer build() {
    return new Answer(protocolIndex.getAndIncrement(), virtual, localizedLabel);
  }

}
