/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service

import ch.ge.ve.jacksonserializer.JSDates
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.time.LocalDateTime
import java.util.regex.Matcher

class TestHelpers {


  static JsonDocumentEntityService jsonDocumentEntityService() {
    return new JsonDocumentEntityService(getObjectMapper())
  }

  static InputStream getResourceStream(Class<?> testClass, String filename) {
    String resourcePath = testClass.getName()
            .replaceAll('\\.[^.]+$', "")
            .replaceAll('\\.', Matcher.quoteReplacement(File.separator)) +
            File.separator +
            filename
    InputStream stream = testClass.getClassLoader().getResourceAsStream(resourcePath)
    if (stream == null) {
      throw new FileNotFoundException(resourcePath)
    }
    return stream
  };

  static String getResourceString(Class<?> testClass, String filename, boolean cleanCrlf = true) {
    String toReturn = getResourceStream(testClass, filename).text
    return cleanCrlf ? toReturn.replaceAll("\r", "") : cleanCrlf
  }

  static byte[] getResourceBytes(String resourcePath) {
    return TestHelpers.getClassLoader().getResourceAsStream(resourcePath).readAllBytes()
  }

  static String getResourceString(String resourcePath, boolean cleanCrlf = true) {
    String toReturn = TestHelpers.getClassLoader().getResourceAsStream(resourcePath).text
    return cleanCrlf ? toReturn.replaceAll("\r", "") : cleanCrlf
  }

  static ObjectMapper getObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper()
    SimpleModule module = new SimpleModule()
    module.addSerializer(LocalDateTime.class, JSDates.SERIALIZER);
    module.addDeserializer(LocalDateTime.class, JSDates.DESERIALIZER);
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    objectMapper.registerModule(module);
  }

}
