/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.asset

import ch.ge.ve.vr.backend.repository.DoiCoatOfArmsRepository
import ch.ge.ve.vr.backend.repository.RawFileRepository
import ch.ge.ve.vr.backend.repository.entity.DoiCoatOfArms
import ch.ge.ve.vr.backend.repository.entity.RawFile
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties
import ch.ge.ve.vr.backend.service.exception.CoatOfArmsNotFoundException
import ch.ge.ve.vr.backend.service.exception.RawFileNotFoundException
import ch.ge.ve.vr.backend.service.exception.TranslationsNotFoundException
import org.springframework.core.io.Resource
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import spock.lang.Specification

class AssetServiceImplTest extends Specification {
  def rawFileRepository = Mock(RawFileRepository)
  def doiCoatOfArmsRepository = Mock(DoiCoatOfArmsRepository)
  def resolver = Mock(PathMatchingResourcePatternResolver)
  def config = new VoteReceiverConfigurationProperties()
  def assetService = new AssetServiceImpl(rawFileRepository, doiCoatOfArmsRepository, config, resolver)

  void setup() {
    config.modelVersion = 2
  }

  def "GetTranslations should succeed when translation exist for a given instance"() {
    given:
    def rawFile = new RawFile()
    rawFile.content = "translations".bytes

    rawFileRepository.findTranslation("test", config.modelVersion) >> Optional.of(rawFile)

    when:
    def translations = assetService.getTranslations("test")

    then:
    translations == "translations"
  }

  def "GetTranslations should fail when translation doesn't exist for a given instance"() {
    given:
    rawFileRepository.findTranslation("test", config.modelVersion) >> Optional.empty()

    when:
    assetService.getTranslations("test")

    then:
    def exception = thrown(TranslationsNotFoundException)
    exception.message == "Protocol instance ID : test"
  }

  def "GetDoiCoatOfArm"() {
    given:
    def doiCoatOfArms = new DoiCoatOfArms()
    doiCoatOfArms.content = "content".bytes
    doiCoatOfArmsRepository.findById("6621") >> Optional.of(doiCoatOfArms)

    when:
    def result = assetService.getDoiCoatOfArm("6621")

    then:
    result == doiCoatOfArms.content
  }

  def "GetDoiCoatOfArm should failed when there is no such coat of arms"() {
    given:
    doiCoatOfArmsRepository.findById("6621") >> Optional.empty()

    when:
    assetService.getDoiCoatOfArm("6621")

    then:
    def exception = thrown(CoatOfArmsNotFoundException)
    exception.message == "No such coat of arm for id 6621"
  }

  def "getFile should failed when there is no file"() {
    given:
    rawFileRepository.findByOperation_ProtocolInstanceIdAndOperation_ModelVersionAndIndex("test", config.modelVersion, 1) >> Optional.empty()

    when:
    assetService.getFile("test", 1)

    then:
    def exception = thrown(RawFileNotFoundException)
    exception.message == "Protocol instance ID : test file index : 1"
  }

  def "LoadPredefinedDoiImages should store image if it wasn't previously defined"() {
    given:
    Resource resource = Mock(Resource)
    resource.filename >> "MU-1245.svg"
    resource.inputStream >> new ByteArrayInputStream("test".bytes)
    resolver.getResources("classpath:doi-coat-of-arms/*") >> [resource]
    doiCoatOfArmsRepository.findById("MU") >> Optional.empty()

    when:
    assetService.loadPredefinedDoiImages()

    then:
    1 * doiCoatOfArmsRepository.save({ DoiCoatOfArms doiCoatOfArms ->
      doiCoatOfArms.id == "1245"
      doiCoatOfArms.content == "test".bytes

    } as DoiCoatOfArms)
  }

  def "LoadPredefinedDoiImages should not store image if it was previously defined"() {
    given:
    Resource resource = Mock(Resource)
    resource.filename >> "MU-1245.svg"
    resource.inputStream >> new ByteArrayInputStream("test".bytes)
    resolver.getResources("classpath:doi-coat-of-arms/*") >> [resource]
    doiCoatOfArmsRepository.findById("MU") >> Optional.of(new DoiCoatOfArms())

    when:
    assetService.loadPredefinedDoiImages()

    then:
    0 * doiCoatOfArmsRepository.save(_ as DoiCoatOfArms)
  }

  def "LoadPredefinedDoiImages should not store image if the format is incorrect"() {
    given:
    Resource resource = Mock(Resource)
    resource.filename >> "thisIsNotGood.svg"
    resource.inputStream >> new ByteArrayInputStream("test".bytes)
    resolver.getResources("classpath:doi-coat-of-arms/*") >> [resource]
    doiCoatOfArmsRepository.findById("thisIsNotGood") >> Optional.of(new DoiCoatOfArms())

    when:
    assetService.loadPredefinedDoiImages()

    then:
    0 * doiCoatOfArmsRepository.save(_ as DoiCoatOfArms)
  }

}
