package ch.ge.ve.vr.backend.service.operation

import static ch.ge.ve.vr.backend.repository.entity.VoterHolder.BirthDateScope.YEAR
import static ch.ge.ve.vr.backend.repository.entity.VoterHolder.BirthDateScope.YEAR_MONTH
import static ch.ge.ve.vr.backend.repository.entity.VoterHolder.BirthDateScope.YEAR_MONTH_DAY

import ch.ge.ve.vr.backend.repository.entity.VoterHolder
import spock.lang.Specification

class BirthDateScopeMapperTest extends Specification {

  def "voter attributes should provide the matching scope"(Integer month, Integer day, VoterHolder.BirthDateScope scope) {
    expect:
    BirthDateScopeMapper.mapToScope(day, month) == scope

    where:
    month | day  | scope
    12    | 31   | YEAR_MONTH_DAY
    12    | null | YEAR_MONTH
    12    | 0    | YEAR_MONTH
    0     | 0    | YEAR
    null  | null | YEAR
  }
}
