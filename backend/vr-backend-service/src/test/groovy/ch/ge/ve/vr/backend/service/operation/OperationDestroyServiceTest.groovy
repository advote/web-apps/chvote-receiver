/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.operation

import ch.ge.ve.vr.backend.repository.OperationRepository
import ch.ge.ve.vr.backend.repository.VoterRepository
import ch.ge.ve.vr.backend.repository.entity.OperationHolder
import spock.lang.Specification

class OperationDestroyServiceTest extends Specification {


  def operationRepository = Mock(OperationRepository)
  def voterRepository = Mock(VoterRepository)
  def service = new OperationDestroyService(operationRepository, voterRepository)

  def "destroy instance should remove voter and operation"() {
    given:

    def operation = new OperationHolder()
    operationRepository.findAllByProtocolInstanceId("pid") >> [
            operation
    ]

    when:
    service.destroyInstance("pid")

    then:
    1 * voterRepository.deleteAllByProtocolInstanceId("pid")
    1 * operationRepository.delete(operation)
  }
}
