package ch.ge.ve.vr.backend.service.operation;

import ch.ge.ve.vr.backend.repository.entity.VoterHolder;

/**
 * Utility class to map the birth date attributes of an imported voter to a scope.
 * <p>
 * The scope is used by the frontend to provide the right validation pattern for the birth date.
 */
public class BirthDateScopeMapper {

  /**
   * Utility class
   */
  private BirthDateScopeMapper() {
  }

  /**
   * Maps the birth date attributes of a voter to a scope. The mapper does not verify if the year, month and days are
   * valid, it just checks for nullity to provide the most relevant scope.
   *
   * @param voterBirthDay   day of birth, eventually null or equals to 0
   * @param voterBirthMonth month of birth, eventually null or equals to 0
   *
   * @return the birth date scope matching the voter
   */
  public static VoterHolder.BirthDateScope mapToScope(Integer voterBirthDay, Integer voterBirthMonth) {
    if (voterBirthDay != null && voterBirthDay != 0) {
      return VoterHolder.BirthDateScope.YEAR_MONTH_DAY;
    } else if (voterBirthMonth != null && voterBirthMonth != 0) {
      return VoterHolder.BirthDateScope.YEAR_MONTH;
    } else {
      return VoterHolder.BirthDateScope.YEAR;
    }
  }
}
