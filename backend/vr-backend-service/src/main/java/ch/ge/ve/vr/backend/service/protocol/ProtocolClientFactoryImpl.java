/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.protocol;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.ProtocolClientImpl;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.vr.backend.service.config.ProtocolConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * A factory for instantiating {@link ProtocolClient}s.
 */
@Service
@Profile("!mocked-protocol")
public class ProtocolClientFactoryImpl implements ProtocolClientFactory {
  private static final Long PROTOCOL_WAIT_TIMEOUT = 1000000L;

  private final RabbitUtilities  rabbit;
  private final SignatureService signatureService;
  private final RandomGenerator  randomGenerator;
  private final ObjectMapper     objectMapper;
  private final int              controlComponentsCount;
  private final int              batchSize;
  private final int              securityLevel;

  private final Map<String, ProtocolClient> clients = new HashMap<>();

  /**
   * Creates a new {@link ProtocolClientFactoryImpl}.
   *
   * @param rabbit                the {@link RabbitUtilities}.
   * @param signatureService      the {@link SignatureService}.
   * @param randomGenerator       the {@link RandomGenerator}.
   * @param objectMapper          the {@link ObjectMapper}.
   * @param protocolConfiguration the configuration properties.
   */
  @Autowired
  public ProtocolClientFactoryImpl(RabbitUtilities rabbit,
                                   SignatureService signatureService,
                                   RandomGenerator randomGenerator,
                                   ObjectMapper objectMapper,
                                   ProtocolConfiguration protocolConfiguration,
                                   @Value("${ch.ge.ve.security-level}") int securityLevel) {
    this.rabbit = rabbit;
    this.signatureService = signatureService;
    this.randomGenerator = randomGenerator;
    this.objectMapper = objectMapper;
    this.controlComponentsCount = protocolConfiguration.getControlComponentsCount();
    this.batchSize = protocolConfiguration.getBatchSize();
    this.securityLevel = securityLevel;
  }

  @Override
  public ProtocolClient client(String protocolId) {

    ProtocolClient client = clients.get(protocolId);

    if (client == null) {

      client = new ProtocolClientImpl(
          securityLevel,
          protocolId,
          controlComponentsCount,
          rabbit,
          signatureService,
          objectMapper,
          randomGenerator,
          PROTOCOL_WAIT_TIMEOUT,
          batchSize);

      clients.put(protocolId, client);
    }

    return client;
  }
}
