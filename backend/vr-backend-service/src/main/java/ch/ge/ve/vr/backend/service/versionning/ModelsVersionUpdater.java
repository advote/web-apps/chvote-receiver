/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.versionning;

import ch.ge.ve.vr.backend.repository.OperationRepository;
import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.operation.OperationImportService;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Service used to update models if the version is outdated
 */
@Service
public class ModelsVersionUpdater {

  private final OperationRepository                 operationRepository;
  private final VoteReceiverConfigurationProperties config;
  private final OperationImportService              importService;

  @Autowired
  public ModelsVersionUpdater(OperationRepository operationRepository,
                              VoteReceiverConfigurationProperties config,
                              OperationImportService importService) {
    this.operationRepository = operationRepository;
    this.config = config;
    this.importService = importService;
  }


  @Transactional
  public void updateOperationModels() {
    operationRepository
        .findOperationsForUpdate(config.getModelVersion())
        .stream()
        .map(OperationHolder::getProtocolInstanceId)
        .distinct()
        .forEach(importService::importOperation);

  }

}

