/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.protocol;

import ch.ge.ve.event.Event;
import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.exception.ProtocolException;
import ch.ge.ve.protocol.client.message.api.MessageHandler;
import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import ch.ge.ve.protocol.client.progress.api.ProgressTracker;
import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriter;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

/**
 * A factory for instantiating {@link ProtocolClient}s.
 */
@Service
@Profile("mocked-protocol")
public class MockProtocolClientFactory implements ProtocolClientFactory {
  private static final Logger logger = LoggerFactory.getLogger(ProtocolService.class);

  private final Map<String, ProtocolClient>         clients = new ConcurrentHashMap<>();
  private final VoteReceiverConfigurationProperties config;
  private final ObjectMapper                        objectMapper;
  private final HttpClient                          httpClient;

  /**
   * Creates a new {@link ProtocolClientFactoryImpl}.
   *
   * @param objectMapper the {@link ObjectMapper}.
   */
  @Autowired
  public MockProtocolClientFactory(VoteReceiverConfigurationProperties config, ObjectMapper objectMapper) {
    this.config = config;
    this.httpClient = HttpClients.createDefault();
    this.objectMapper = objectMapper;
  }

  /**
   * Create a new {@link ProtocolClient} instance.
   *
   * @param protocolId the id of the protocol instance.
   *
   * @return a new {@link ProtocolClient}.
   */
  @Override
  public ProtocolClient client(String protocolId) {
    return clients.computeIfAbsent(protocolId, MockedProtocolClient::new);
  }

  private class MockedProtocolClient implements ProtocolClient {
    private final String protocolId;

    MockedProtocolClient(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public List<ObliviousTransferResponse> publishBallot(int voterIndex, BallotAndQuery ballotAndQuery) {
      try {
        HttpPost
            request = new HttpPost(
            String.format("%s/protocol/submit-ballot/%s/%s", config.getMockEndpoint(), protocolId, voterIndex));
        StringEntity entity = new StringEntity(objectMapper.writeValueAsString(ballotAndQuery));
        request.setEntity(entity);
        request.setHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        request.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        return objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()),
                                      new TypeReference<List<ObliviousTransferResponse>>() {
                                      });
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }

    @Override
    public List<FinalizationCodePart> submitConfirmation(int voterId, Confirmation confirmation)
        throws ConfirmationFailedException {
      try {
        HttpPost request = new HttpPost(
            String.format("%s/protocol/submit-confirmation-code/%s/%s", config.getMockEndpoint(), protocolId, voterId));
        StringEntity entity = new StringEntity(objectMapper.writeValueAsString(confirmation));
        request.setEntity(entity);
        request.setHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        request.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        return objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()),
                                      new TypeReference<List<FinalizationCodePart>>() {
                                      });
      } catch (HttpResponseException e) {
        logger.info("Confirmation code failed for protocolId:" + protocolId + " and voter id:" + voterId, e);
        throw new ConfirmationFailedException();
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }


    @Override
    public PublicParameters fetchPublicParameters() {
      HttpGet request = new HttpGet(String.format("%s/protocol/public-parameters", config.getMockEndpoint()));
      try {
        return objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()), PublicParameters.class);
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }

    @Override
    public ElectionSetForVerification fetchElectionSet() {
      HttpGet request = new HttpGet(String.format("%s/election-set/%s", config.getMockEndpoint(), protocolId));
      try {
        return objectMapper
            .readValue(httpClient.execute(request, new BasicResponseHandler()), new TypeReference<List<BigInteger>>() {
            });
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }

    @Override
    public List<BigInteger> fetchPrimes() {
      HttpGet request = new HttpGet(String.format("%s/protocol/primes/%s", config.getMockEndpoint(), protocolId));
      try {
        return objectMapper
            .readValue(httpClient.execute(request, new BasicResponseHandler()), new TypeReference<List<BigInteger>>() {
            });
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }

    @Override
    public Map<Integer, EncryptionPublicKey> fetchPublicKeyParts() {
      HttpGet request = new HttpGet(String.format("%s/protocol/public-keys/%s", config.getMockEndpoint(), protocolId));
      try {
        List<EncryptionPublicKey> encryptionPublicKeys =
            objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()),
                                   new TypeReference<List<EncryptionPublicKey>>() {
                                   });
        AtomicInteger atomicInteger = new AtomicInteger();
        return encryptionPublicKeys.stream().collect(Collectors.toMap(o -> atomicInteger.incrementAndGet(), o -> o));
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }

    @Override
    public EncryptionPublicKey fetchElectionOfficerKey() {
      HttpGet request = new HttpGet(String.format("%s/protocol/officer-public-key", config.getMockEndpoint()));
      try {
        return
            objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()), EncryptionPublicKey.class);
      } catch (IOException e) {
        throw new ProtocolException(e);
      }
    }


    @Override
    public MessageHandler<Event> createDeadLetterHandler(Class eventClass) {
      throw new UnsupportedOperationException();
    }

    @Override
    public String getProtocolId() {
      return protocolId;
    }

    @Override
    public void registerPrintingAuthorityPrivateKey(String authorityName, IdentificationPrivateKey privateKey) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void setPublicParameters(PublicParameters publicParameters) {
      throw new UnsupportedOperationException();
    }

    @Override
    public PublicParameters sendPublicParameters(PublicParametersFactory securityLevel) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void sendPublicParameters(PublicParameters publicParameters) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePublicParametersForwardedToCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void requestKeyGeneration() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePublicKeyPartsPublishedToBB(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePublicKeyPartsForwardedToCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void sendElectionOfficerPublicKey(EncryptionPublicKey publicKey) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureElectionOfficerPublicKeyForwardedToCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureControlComponentsReady(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void reinitFromProtocolState() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void sendElectionSet(ElectionSetWithPublicKey electionSet) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureElectionSetForwardedToCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void sendVoters(List<Voter> voters, int batchSize) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureVotersForwardedToCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePublicCredentialsBuiltForAllCCs(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePrimesSentToBB(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void requestPrivateCredentials(Path outputFolder, Consumer<Path> printerFileConsumer, ProgressTrackerByCC
        tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePrivateCredentialsPublished() {
      throw new UnsupportedOperationException();
    }

    @Override
    public int decryptPrinterFiles(Path printerFilesFolder) {
      throw new UnsupportedOperationException();
    }

    @Override
    public Map<Integer, VotePerformanceStats> castVotes(int votesCount, Path printerFilesFolder) {
      throw new UnsupportedOperationException();
    }

    @Override
    public long getVotesCastCount() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void requestShufflingAndPartialDecryption() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureShufflesPublished(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensurePartialDecryptionsPublished(ProgressTracker tracker) {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean checkTally(EncryptionPrivateKey electionOfficerPrivateKey) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void writeProtocolAuditLog(ElectionVerificationDataWriter verificationDataWriter) {
      throw new UnsupportedOperationException();
    }


    @Override
    public void requestCleanup() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void createHash() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void createGeneralAlgorithms() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void createChannelSecurityAlgorithms() {
      throw new UnsupportedOperationException();
    }

    @Override
    public Tally getExpectedTally() {
      throw new UnsupportedOperationException();
    }

    @Override
    public void ensureAllSentEventsHaveBeenAcknowledged() {
      throw new UnsupportedOperationException();
    }
  }
}
