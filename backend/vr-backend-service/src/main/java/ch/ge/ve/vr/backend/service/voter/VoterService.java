/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.service.voter;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.lang.Integer.parseInt;

import ch.ge.ve.crypto.authentication.HashedAuthentication;
import ch.ge.ve.crypto.impl.authentication.AuthenticationHasherImpl;
import ch.ge.ve.vr.backend.repository.VoterRepository;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import ch.ge.ve.vr.backend.service.exception.BirthDateCheckException;
import ch.ge.ve.vr.backend.service.exception.ConfirmationCheckException;
import ch.ge.ve.vr.backend.service.exception.TechnicalException;
import ch.ge.ve.vr.backend.service.exception.UnknownVoterException;
import ch.ge.ve.vr.backend.service.exception.VerificationCodeRevealedException;
import java.security.SecureRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service related to voters
 */
@Service
public class VoterService {

  private static final Integer MAX_CONFIRMATION_CHECK_FAILURE = 5;
  private static final Integer MAX_BIRTHDAY_CHECK_FAILURE     = 5;
  private static final String  SEPARATOR_BIRTHDAY_DATE        = ".";
  private static final int     ITERATIONS_MIN                 = 10000;
  private static final int     ITERATIONS_MAX                 = 11000;
  private static final String  HASH_SEPARATOR                 = ";"; //separator used in the hash pattern
  private static final String  HASH_DATEOFBIRTH_PATTERN       =
      "^([a-fA-F0-9]+)" + HASH_SEPARATOR + "([0-9]+)" + HASH_SEPARATOR + "([a-fA-F0-9]+)$";
  private static final int     SALT                           = 1;
  private static final int     ITERATIONS                     = 2;
  private static final int     HASH                           = 3;

  private final VoterRepository voterRepository;

  public VoterService(VoterRepository voterRepository) {
    this.voterRepository = voterRepository;
  }

  /**
   * Return a voter from a protocol instance
   *
   * @param protocolInstanceId protocol instance identifier
   * @param voterId            voter identifier
   *
   * @throws UnknownVoterException if user is unknown for this protocol instance
   */
  public VoterHolder getVoter(String protocolInstanceId, Integer voterId) {
    return voterRepository.findByProtocolInstanceIdAndVoterId(protocolInstanceId, voterId)
                          .orElseThrow(UnknownVoterException::unknownVoter);
  }

  /**
   * Mark failed confirmation on voter
   *
   * @param voter the voter
   *
   * @throws ConfirmationCheckException after updating database
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = ConfirmationCheckException.class)
  public void markVoterConfirmationFailed(VoterHolder voter) {
    voter.setConfirmationCheckFailed(voter.getConfirmationCheckFailed() + 1);
    voterRepository.save(voter);

    checkVoterConfirmationAllowed(voter);
    throw ConfirmationCheckException.wrongConfirmationCodeSubmitted(getNumberOfCheckLeft(voter));
  }

  private int getNumberOfCheckLeft(VoterHolder voter) {
    return MAX_CONFIRMATION_CHECK_FAILURE - voter.getConfirmationCheckFailed();
  }

  /**
   * Throw a BirthDateCheckException if a voter is not allowed to authenticate.
   *
   * @param voter the voter
   *
   * @throws BirthDateCheckException when user is not allowed to authenticate
   */
  public void checkVoterAuthenticationAllowed(VoterHolder voter) {
    if (voter.getBirthDayCheckFailed() >= MAX_BIRTHDAY_CHECK_FAILURE) {
      throw BirthDateCheckException.maxNumberOfChecksReached(MAX_BIRTHDAY_CHECK_FAILURE);
    }
  }

  /**
   * Throw a ConfirmationCheckException if a voter is not allowed to confirm.
   *
   * @param voter the voter
   *
   * @throws ConfirmationCheckException when user is not allowed to confirm
   */
  public void checkVoterConfirmationAllowed(VoterHolder voter) {
    if (voter.getConfirmationCheckFailed() >= MAX_CONFIRMATION_CHECK_FAILURE) {
      throw ConfirmationCheckException.maxNumberOfChecksReached(MAX_CONFIRMATION_CHECK_FAILURE);
    }
  }

  /**
   * Perform a check on birth date
   *
   * @param protocolInstanceId the protocol's instance ID
   * @param voterId            the voter's ID
   * @param year               birth year
   * @param month              birth month (optional)
   * @param day                birth (optional)
   *
   * @throws BirthDateCheckException when birth date check failed
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = BirthDateCheckException.class)
  public void checkBirthDate(String protocolInstanceId, int voterId, Integer year, Integer month,
                             Integer day) {
    VoterHolder voter = getVoter(protocolInstanceId, voterId);

    checkVoterAuthenticationAllowed(voter);

    if (!isStoredDateValidAndEqualsToInformationProvidedByVoter(voter, day, month, year)) {
      voter.setBirthDayCheckFailed(voter.getBirthDayCheckFailed() + 1);
      voterRepository.save(voter);
      checkVoterAuthenticationAllowed(voter);
      throw BirthDateCheckException
          .wrongBirthDateSubmitted(MAX_BIRTHDAY_CHECK_FAILURE - voter.getBirthDayCheckFailed());
    }

  }

  /**
   * @param voter voter configuration for a given operation
   * @param day   voter provided day of birth
   * @param month voter provided month of birth
   * @param year  voter provided year of birth
   *
   * @return true if the date is valid and equal to the information provided by the voter
   */
  private boolean isStoredDateValidAndEqualsToInformationProvidedByVoter(VoterHolder voter,
                                                                         Integer day, Integer month, Integer year) {
    String voterDateOfBirthHashed = voter.getVoterBirthHashed();
    String dateOfBirthHashed = hashDateOfBirth(day, month, year);
    return   //is valid
        isValidHash(voterDateOfBirthHashed) && isValidHash(dateOfBirthHashed) &&
        //hash are equals
        dateAndHashAreSame(day, month, year, voterDateOfBirthHashed);
  }

  /**
   * comparison of the given hash with the one created with the parameters
   *
   * @param day                    day of birth
   * @param month                  month of birth
   * @param year                   year of birth
   * @param voterDateOfBirthHashed hash of the voter's date of birth
   *
   * @return true if the hashes are equal
   */
  private boolean dateAndHashAreSame(Integer day, Integer month, Integer year, String voterDateOfBirthHashed) {

    //date hashing with the same salt and iteration as the given voter's hash
    String dateOfBirthHashed = hashDateOfBirth(day, month, year,
                                               toByte(getSalt(voterDateOfBirthHashed)),
                                               parseInt(getIterations(voterDateOfBirthHashed)));

    return getHash(dateOfBirthHashed).equals(getHash(voterDateOfBirthHashed));
  }

  /**
   * hash format validation
   *
   * @param dateOfBirthHashed a string to compare with the HASH pattern
   *
   * @return true if the hash format is correct
   */
  protected boolean isValidHash(String dateOfBirthHashed) {
    Matcher validMatcher = getValidMatcher(dateOfBirthHashed);
    return validMatcher != null && validMatcher.matches();
  }

  /**
   * gives a valid Matcher for the hashed date passed in parameter
   *
   * @param dateOfBirthHashed hashed date to be used for creation
   *
   * @return a Matcher object
   */
  private Matcher getValidMatcher(String dateOfBirthHashed) {
    if (isNullOrEmpty(dateOfBirthHashed)) {
      return null;
    }
    return Pattern.compile(HASH_DATEOFBIRTH_PATTERN).matcher(dateOfBirthHashed);
  }

  /**
   * extracts the part corresponding to the salt, in the date passed in parameter
   *
   * @param dateOfBirthHashed hashed date to be used extraction
   *
   * @return the salt
   */
  protected String getSalt(String dateOfBirthHashed) {
    return getPartOfBirthHashed(dateOfBirthHashed, SALT);
  }

  /**
   * extracts the part corresponding to the iterations number, in the date passed in parameter
   *
   * @param dateOfBirthHashed hashed date to be used extraction
   *
   * @return the iterations number
   */
  private String getIterations(String dateOfBirthHashed) {
    return getPartOfBirthHashed(dateOfBirthHashed, ITERATIONS);
  }

  /**
   * extracts the part corresponding to the hash, in the date passed in parameter
   *
   * @param dateOfBirthHashed hashed date to be used extraction
   *
   * @return the hash
   */
  protected String getHash(String dateOfBirthHashed) {
    return getPartOfBirthHashed(dateOfBirthHashed, HASH);
  }

  private String getPartOfBirthHashed(String dateOfBirthHashed, int requiredInformation) {
    String result = "";
    if (isNullOrEmpty(dateOfBirthHashed)) {
      return result;
    }
    Matcher m = getValidMatcher(dateOfBirthHashed);
    if (m != null && m.matches() && m.groupCount() == HASH) {
      result = m.group(requiredInformation);
    }
    return result;
  }

  /**
   * hashing of date of birth as defined in chvote-crypto
   * salt and iterations are taken in chvote-crypto
   *
   * @param dateOfBirthDay   day of birth
   * @param dateOfBirthMonth month of birth
   * @param dateOfBirthYear  year of birth
   *
   * @return a chain with salt and hash, in base64 hexadecimal representation, with separator between them
   */
  public String hashDateOfBirth(Integer dateOfBirthDay, Integer dateOfBirthMonth, Integer dateOfBirthYear) {
    AuthenticationHasherImpl authenticationHasher = new AuthenticationHasherImpl();
    return hashDateOfBirth(dateOfBirthDay, dateOfBirthMonth, dateOfBirthYear,
                           authenticationHasher.getSalt(), getIterations());
  }

  /**
   * hashing of date of birth as defined in chvote-crypto
   * salt and iterations are provided
   *
   * @param dateOfBirthDay   day of birth
   * @param dateOfBirthMonth month of birth
   * @param dateOfBirthYear  year of birth
   * @param salt             salt value
   * @param iterations       iterations number
   *
   * @return a chain with salt and hash, in base64 hexadecimal representation, with separator between them
   */
  private String hashDateOfBirth(Integer dateOfBirthDay, Integer dateOfBirthMonth, Integer dateOfBirthYear,
                                 byte[] salt, int iterations) {
    AuthenticationHasherImpl authenticationHasher = new AuthenticationHasherImpl();
    HashedAuthentication hashedAuthentication = authenticationHasher
        .hash(getStringDateOfBirth(dateOfBirthDay, dateOfBirthMonth, dateOfBirthYear),
              salt,
              iterations);

    String resultSalt = toHexa(hashedAuthentication.salt());
    String resultIterations = String.valueOf(hashedAuthentication.iterations());
    String hash = toHexa(hashedAuthentication.hash());

    return String.format("%s%s%s%s%s", resultSalt, HASH_SEPARATOR, resultIterations, HASH_SEPARATOR, hash);
  }

  /**
   * Converts a byte buffer into an array of characters representing the hexadecimal values of each byte in order.
   * The returned array will be double the length of the passed array, as it takes two characters to represent any
   * given byte
   *
   * @param array a byte[] to convert to Hex characters
   *
   * @return A String containing lower-case hexadecimal characters
   */
  protected String toHexa(byte[] array) {
    return Hex.encodeHexString(array);
  }

  /**
   * Converts a String representing hexadecimal values into an array of bytes of those same values
   *
   * @param hex A String containing hexadecimal digits
   *
   * @return A byte array containing binary data decoded from the supplied string
   */
  protected byte[] toByte(String hex) {
    try {
      return Hex.decodeHex(hex);
    } catch (DecoderException e) {
      throw new TechnicalException(e);
    }
  }

  private String getStringDateOfBirth(Integer dateOfBirthDay, Integer dateOfBirthMonth, Integer dateOfBirthYear) {
    return dateOfBirthDay + SEPARATOR_BIRTHDAY_DATE + dateOfBirthMonth + SEPARATOR_BIRTHDAY_DATE + dateOfBirthYear;
  }

  /**
   * calculates a random iteration number for the hash function
   *
   * @return an integer between min and max constants
   */
  private int getIterations() {
    return new SecureRandom().nextInt(ITERATIONS_MAX - ITERATIONS_MIN + 1) + ITERATIONS_MIN;
  }

  /**
   * Mark a voting card with a flag that indicates that verification codes have been revealed
   * Therefor it is no longer possible to vote by internet
   *
   * @param protocolId the protocol's instance ID
   * @param voterId    the voter's ID
   */
  @Transactional
  public void markVerificationCodeRevealed(String protocolId, Integer voterId) {
    VoterHolder voter = getVoter(protocolId, voterId);
    voter.setVerificationCodeRevealed(true);
    voterRepository.save(voter);
  }


  /**
   * Throw a VerificationCodeRevealedException if finalization codes has already been revealed
   *
   * @param voter the voter
   *
   * @throws VerificationCodeRevealedException when user is not allowed to authenticate
   */
  public void checkVerificationCodeNotRevealed(VoterHolder voter) {
    if (voter.isVerificationCodeRevealed()) {
      throw new VerificationCodeRevealedException();
    }
  }


}
