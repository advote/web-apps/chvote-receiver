/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend;

import ch.ge.ve.chvote.pact.b2b.client.PactB2BClient;
import ch.ge.ve.chvote.pact.b2b.client.PactB2BRestClient;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.AcknowledgementServiceInMemoryImpl;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.service.SignatureServiceImpl;
import ch.ge.ve.vr.backend.service.config.PactConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Map;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * Utilities class that declare all backend services
 */

@Configuration
@ComponentScan({"ch.ge.ve.config", "ch.ge.ve.service", "ch.ge.ve.vr.backend.service"})
public class BackendServices {

  @Bean
  public PactB2BClient pactB2BClient(PactConfiguration pactConfiguration, RestTemplateBuilder restTemplate) {
    return new PactB2BRestClient(pactConfiguration, restTemplate);
  }

  @Bean
  public PathMatchingResourcePatternResolver pathMatchingResourcePatternResolver() {
    return new PathMatchingResourcePatternResolver();
  }

  @Bean
  public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
    return new RabbitAdmin(connectionFactory);
  }

  @Bean
  public RabbitUtilities rabbitUtilities(EventBus eventBus, RabbitAdmin rabbitAdmin,
                                         MessageConverter messageConverter) {
    return new RabbitUtilities(eventBus, rabbitAdmin, messageConverter);
  }

  @Bean
  public RandomGenerator randomGenerator(AlgorithmsSpec algorithmsSpec) {
    return RandomGenerator
        .create(algorithmsSpec.getRandomGeneratorAlgorithm(), algorithmsSpec.getRandomGeneratorProvider());
  }

  @Bean
  public AcknowledgementService acknowledgementService() {
    return new AcknowledgementServiceInMemoryImpl();
  }

  @Bean
  public SignatureService signatureService(IdentificationPrivateKey signingKey,
                                           Map<String, Map<String, BigInteger>> verificationKeys,
                                           AlgorithmsSpec algorithmsSpec, RandomGenerator randomGenerator,
                                           ObjectMapper objectMapper,
                                           PublicParameters publicParameters) {
    return new SignatureServiceImpl(Signatories.CHVOTE, signingKey, verificationKeys,
                                    algorithmsSpec, randomGenerator, objectMapper,
                                    id -> publicParameters);
  }

  @Bean
  public IdentificationPrivateKey signingKey(
      @Value("${protocol.signing-key.url}") URL signingKeyUrl,
      @Value("${protocol.signing-key.password}") char[] signingKeyPassword,
      ObjectMapper objectMapper) {
    // TODO: protect key with a password
    try {
      return objectMapper.readValue(signingKeyUrl, IdentificationPrivateKey.class);
    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read signing key", e);
    }
  }

  @Bean
  PublicParameters publicParameters(@Value("${ch.ge.ve.security-level}") int securityLevel,
                                    @Value("${protocol.control-components-count}") int ccCount) {
    return PublicParametersFactory.forLevel(securityLevel).createPublicParameters(ccCount);
  }

}
