/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.interceptor

import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod
import ch.ge.ve.vr.backend.fixtures.UseCaseBuilder
import ch.ge.ve.vr.backend.rest.exception.SiteIsClosedException
import ch.ge.ve.vr.backend.service.exception.TechnicalException
import ch.ge.ve.vr.backend.service.operation.OperationService
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import org.springframework.web.method.HandlerMethod
import spock.lang.Specification

class CheckSiteIsOpenInterceptorTest extends Specification {


  def operationService = Mock(OperationService)
  def checkSiteIsOpenInterceptor = new CheckSiteIsOpenInterceptor(operationService)
  def request = Mock(HttpServletRequest)
  def handlerMethod = Mock(HandlerMethod)


  def "preHandle should throw an exception if protocol instance id is not found on an annotated method"() {
    given:
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    request.getParameter("test") >> null
    request.getRequestURI() >> "www.test.com"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen

    when:
    checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    def ex = thrown(TechnicalException)
    ex.message == "Could not find parameter test in request www.test.com"
  }

  def "preHandle should pass if there is no CheckSiteIsOpen annotation"() {
    when:
    def value = checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    value
  }


  def "preHandle should pass if voting period is open"() {
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    request.getParameter("test") >> "pid"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen
    operationService.getOperationWithoutOperationCheckAccess("pid") >>
            new UseCaseBuilder()
                    .forSimulation()
                    .votingPeriod(new VotingPeriod(LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(1), 10))
                    .build().operation

    when:
    def value = checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    value
  }


  def "preHandle should pass if voting period is close but grace period is still open and grace period is taken into account"() {
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    checkSiteIsOpen.allowedDuringGracePeriod() >> true
    request.getParameter("test") >> "pid"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen

    operationService.getOperationWithoutOperationCheckAccess("pid") >>
            new UseCaseBuilder()
                    .forSimulation()
                    .votingPeriod(new VotingPeriod(LocalDateTime.now().minusDays(1), LocalDateTime.now().minusMinutes(1), 10))
                    .build().operation

    when:
    def value = checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    value
  }

  def "preHandle should not pass if voting period is close but grace period is still open and grace period is not taken into account"() {
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    checkSiteIsOpen.allowedDuringGracePeriod() >> false
    request.getParameter("test") >> "pid"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen

    operationService.getOperationWithoutOperationCheckAccess("pid") >>
            new UseCaseBuilder()
                    .forSimulation()
                    .votingPeriod(new VotingPeriod(LocalDateTime.now().minusDays(1), LocalDateTime.now().minusMinutes(1), 10))
                    .build().operation

    when:
    checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    def ex = thrown(SiteIsClosedException)
    ex.messageKey == "site-is-closed"
  }


  def "preHandle should not pass if voting period is closed"() {
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    checkSiteIsOpen.allowedDuringGracePeriod() >> false
    request.getParameter("test") >> "pid"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen

    operationService.getOperationWithoutOperationCheckAccess("pid") >>
            new UseCaseBuilder()
                    .forSimulation()
                    .votingPeriod(new VotingPeriod(LocalDateTime.now().minusDays(2), LocalDateTime.now().minusDays(1), 10))
                    .build().operation

    when:
    checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    def ex = thrown(SiteIsClosedException)
    ex.messageKey == "site-is-closed"
  }

  def "preHandle should not pass if voting period is not yet open"() {
    def checkSiteIsOpen = Mock(CheckSiteIsOpen)
    checkSiteIsOpen.value() >> "test"
    checkSiteIsOpen.allowedDuringGracePeriod() >> false
    request.getParameter("test") >> "pid"
    handlerMethod.getMethodAnnotation(CheckSiteIsOpen) >> checkSiteIsOpen

    operationService.getOperationWithoutOperationCheckAccess("pid") >>
            new UseCaseBuilder()
                    .forSimulation()
                    .votingPeriod(new VotingPeriod(LocalDateTime.now().plusDays(1), LocalDateTime.now().plusDays(2), 10))
                    .build().operation

    when:
    checkSiteIsOpenInterceptor.preHandle(request, null, handlerMethod);

    then:
    def ex = thrown(SiteIsClosedException)
    ex.messageKey == "site-is-not-open"
  }


}
