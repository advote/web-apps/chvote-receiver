/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.web.controller;

import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.model.OperationStatus;
import ch.ge.ve.vr.backend.service.model.OperationSummary;
import ch.ge.ve.vr.backend.service.operation.OperationService;
import ch.ge.ve.vr.backend.web.model.Canton;
import ch.ge.ve.vr.backend.web.model.OperationSummaries;
import ch.ge.ve.vr.backend.web.model.WebLink;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Controller for the chvote-receiver home pages
 */
@Controller
public class HomePageController {

  private final        OperationService                    service;
  private final        SessionLocaleResolver               sessionLocaleResolver;
  private final        VoteReceiverConfigurationProperties config;
  private static final String                              TEST_CONTEXT = "/test";
  private static final String                              ERROR        = "error";
  private static final String                              BASE_URL     = "baseUrl";
  private static final String                              CONTEXT      = "context";
  private static final String                              CANTONS      = "cantons";
  private static final String                              INDEX        = "index";
  private static final String                              VO           = "vo";
  private static final String                              FOOTER_LINKS = "footerLinks";
  private static final String                              INDEX_CANTON = "index-canton";

  @Autowired
  public HomePageController(OperationService service,
                            SessionLocaleResolver sessionLocaleResolver,
                            VoteReceiverConfigurationProperties config) {
    this.service = service;
    this.sessionLocaleResolver = sessionLocaleResolver;
    this.config = config;
  }

  /**
   * Handles the root Home page, consisting in a list of Cantons with their homepage links
   *
   * @return the root index page
   */
  @GetMapping(value = "/")
  public String index(HttpServletRequest request, Model model) {
    model.addAttribute(CANTONS, Canton.values());
    model.addAttribute(BASE_URL, config.getBaseUrl());
    model.addAttribute(CONTEXT, request.getServletPath());
    return INDEX;
  }

  /**
   * Handles a Canton Home page
   *
   * @return the Canton's operations index
   */
  @GetMapping(value = {"/{canton}/{lang}", "/{canton}"})
  public String indexCanton(org.springframework.ui.Model model,
                            @PathVariable String canton,
                            @PathVariable(required = false) String lang,
                            HttpServletRequest request,
                            HttpServletResponse response) {
    // check canton; if invalid, redirect to error page
    if (Canton.byCode(canton) == null) {
      return ERROR;
    }

    HomePageHelper.checkAndSetLocale(lang, canton, request, response, sessionLocaleResolver);

    canton = canton.toUpperCase(Locale.getDefault());
    final Map<OperationStatus, List<OperationSummary>>
        allOperationSummaries =
        service.findOperations(canton, TEST_CONTEXT.equals(request.getServletPath()));
    OperationSummaries operationSummaries = HomePageHelper.buildOperationSummariesVo(request.getServletPath(),
                                                                                     allOperationSummaries,
                                                                                     canton,
                                                                                     config);
    model.addAttribute(BASE_URL, config.getBaseUrl());
    model.addAttribute(CONTEXT, request.getServletPath());
    model.addAttribute(VO, operationSummaries);
    model.addAttribute(FOOTER_LINKS, getFooterLinks());

    return INDEX_CANTON;
  }

  private List<WebLink> getFooterLinks() {
    return Collections.emptyList();
  }

}
