/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.controller;

import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.vr.backend.rest.model.VoterBallotInformation;
import ch.ge.ve.vr.backend.service.protocol.ProtocolService;
import ch.ge.ve.vr.backend.service.voter.VoterService;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller exposing REST api to call protocol service
 */
@RestController
@RequestMapping("/protocol")
public class ProtocolController {

  private final ProtocolService protocolService;
  private final VoterService    voterService;

  @Autowired
  public ProtocolController(ProtocolService protocolService, VoterService voterService) {
    this.protocolService = protocolService;
    this.voterService = voterService;
  }

  @GetMapping(value = "/public-parameters")
  public PublicParameters getPublicParameters(@RequestParam("protocolInstanceId") String protocolId) {
    return protocolService.getPublicParameters(protocolId);
  }

  @GetMapping(value = "/primes")
  public List<BigInteger> getPrimes(@RequestParam("protocolInstanceId") String protocolId) {
    return protocolService.getPrimes(protocolId);
  }

  @GetMapping(value = "/public-keys")
  public List<EncryptionPublicKey> getPublicKeys(@RequestParam("protocolInstanceId") String protocolId) {
    return protocolService.getPublicKeys(protocolId);
  }

  @PostMapping(value = "/submit-ballot")
  public List<ObliviousTransferResponse> submitBallot(@RequestBody VoterBallotInformation voterBallotInformation,
                                                      @RequestParam("protocolInstanceId") String protocolId,
                                                      @RequestParam("voterId") Integer voterId) {

    voterService.checkBirthDate(protocolId, voterId, voterBallotInformation.getBirthDateYear(),
                                voterBallotInformation.getBirthDateMonth(),
                                voterBallotInformation.getBirthDateDay());

    return protocolService.submitBallot(voterBallotInformation.getBallotAndQuery(), protocolId, voterId);
  }

  @PostMapping(value = "/submit-confirmation-code")
  public List<FinalizationCodePart> submitConfirmationCode(@RequestBody Confirmation confirmation,
                                                           @RequestParam("protocolInstanceId") String protocolId,
                                                           @RequestParam("voterId") Integer voterId) {
    return protocolService.submitConfirmationCode(confirmation, protocolId, voterId);
  }
}
