/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.web.model;

import com.google.common.base.Preconditions;
import java.util.Arrays;
import java.util.Locale;
import javax.validation.constraints.NotNull;

/**
 * Canton enum
 */
public enum Canton {
  AG(Locale.GERMAN),
  BE(Locale.GERMAN, Locale.FRENCH),
  BS(Locale.GERMAN),
  GE(Locale.FRENCH),
  LU(Locale.GERMAN),
  SG(Locale.GERMAN),
  VD(Locale.FRENCH);

  private final Locale defaultLocale;
  private       Locale secondLocale;

  Canton(@NotNull Locale defaultLocale) {
    Preconditions.checkNotNull(defaultLocale);
    this.defaultLocale = defaultLocale;
  }

  Canton(@NotNull Locale defaultLocale, Locale secondLocale) {
    this(defaultLocale);
    this.secondLocale = secondLocale;
  }

  public Locale getDefaultLocale() {
    return defaultLocale;
  }

  public Locale getSecondLocale() {
    return secondLocale;
  }

  public static Canton byCode(String cantonCode) {
    return Arrays.stream(Canton.values())
                 .filter(canton -> canton.toString().equalsIgnoreCase(cantonCode))
                 .findFirst()
                 .orElse(null);
  }
}
