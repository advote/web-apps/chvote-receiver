/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.rest.exception;

import ch.ge.ve.vr.backend.service.exception.BusinessException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Exception thrown when voting site is closed.
 * Site is closed when operation is not yet started or when its closing date is already reached.
 */
public class SiteIsClosedException extends BusinessException {

  private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
  private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");


  public SiteIsClosedException(String messageKey, LocalDateTime openingDate, LocalDateTime closingDate) {
    super(messageKey,
          openingDate.format(DATE_FORMATTER), openingDate.format(TIME_FORMATTER),
          closingDate.format(DATE_FORMATTER), closingDate.format(TIME_FORMATTER));
  }
}
