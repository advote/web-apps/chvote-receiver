/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.web.model;

import ch.ge.ve.vr.backend.service.model.OperationSummary;
import java.util.List;

/**
 * Vo used to display the operations index for a given Canton
 */
public class OperationSummaries {
  private boolean                noOperation;
  private List<OperationSummary> pastOperationSummaries;
  private List<OperationSummary> inProgressOperationSummaries;
  private List<OperationSummary> futureOperationSummaries;
  private List<OperationSummary> settingsTestOperationSummaries;
  private String                 voteUrl;
  private String                 changeLangUrl;
  private String                 canton;

  public boolean isNoOperation() {
    return noOperation;
  }

  public void setNoOperation(boolean noOperation) {
    this.noOperation = noOperation;
  }

  public List<OperationSummary> getPastOperationSummaries() {
    return pastOperationSummaries;
  }

  public void setPastOperationSummaries(List<OperationSummary> pastOperationSummaries) {
    this.pastOperationSummaries = pastOperationSummaries;
  }

  public List<OperationSummary> getInProgressOperationSummaries() {
    return inProgressOperationSummaries;
  }

  public void setInProgressOperationSummaries(List<OperationSummary> inProgressOperationSummaries) {
    this.inProgressOperationSummaries = inProgressOperationSummaries;
  }

  public List<OperationSummary> getFutureOperationSummaries() {
    return futureOperationSummaries;
  }

  public void setFutureOperationSummaries(List<OperationSummary> futureOperationSummaries) {
    this.futureOperationSummaries = futureOperationSummaries;
  }

  public List<OperationSummary> getSettingsTestOperationSummaries() {
    return settingsTestOperationSummaries;
  }

  public void setSettingsTestOperationSummaries(List<OperationSummary> settingsTestOperationSummaries) {
    this.settingsTestOperationSummaries = settingsTestOperationSummaries;
  }

  public String getVoteUrl() {
    return voteUrl;
  }

  public void setVoteUrl(String voteUrl) {
    this.voteUrl = voteUrl;
  }

  public String getChangeLangUrl() {
    return changeLangUrl;
  }

  public void setChangeLangUrl(String changeLangUrl) {
    this.changeLangUrl = changeLangUrl;
  }

  public String getCanton() {
    return canton;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }
}
