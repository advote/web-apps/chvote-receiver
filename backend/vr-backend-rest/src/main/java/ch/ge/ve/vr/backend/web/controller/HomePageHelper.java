/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.web.controller;

import ch.ge.ve.chvote.pact.b2b.client.model.Lang;
import ch.ge.ve.vr.backend.service.config.VoteReceiverConfigurationProperties;
import ch.ge.ve.vr.backend.service.model.OperationStatus;
import ch.ge.ve.vr.backend.service.model.OperationSummary;
import ch.ge.ve.vr.backend.web.model.Canton;
import ch.ge.ve.vr.backend.web.model.OperationSummaries;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Class assembling the objects returned by the controller
 */
public class HomePageHelper {
  private HomePageHelper() {
  }

  public static OperationSummaries buildOperationSummariesVo(String context,
                                                             Map<OperationStatus, List<OperationSummary>> operationSummaries,
                                                             String canton,
                                                             VoteReceiverConfigurationProperties config) {
    OperationSummaries operations = new OperationSummaries();
    operations.setPastOperationSummaries(operationSummaries.get(OperationStatus.PAST));
    operations.setInProgressOperationSummaries(operationSummaries.get(OperationStatus.IN_PROGRESS));
    operations.setFutureOperationSummaries(operationSummaries.get(OperationStatus.FUTURE));
    operations.setSettingsTestOperationSummaries(operationSummaries.get(OperationStatus.IN_TEST));
    operations.setVoteUrl(config.getVoteUrl());
    operations.setChangeLangUrl(
        String.format("%s%s/%s/", config.getBaseUrl(), context, canton.toLowerCase(Locale.getDefault())));
    operations.setCanton(canton.toLowerCase(Locale.getDefault()));
    operations.setNoOperation((operations.getPastOperationSummaries().size() +
                               operations.getInProgressOperationSummaries().size() +
                               operations.getFutureOperationSummaries().size() +
                               operations.getSettingsTestOperationSummaries().size()) == 0);

    return operations;
  }

  static void checkAndSetLocale(@PathVariable String lang, String canton,
                                HttpServletRequest request,
                                HttpServletResponse response,
                                SessionLocaleResolver sessionLocaleResolver) {
    final Locale locale = isValidLang(lang) ? Locale.forLanguageTag(lang) : Canton.byCode(canton).getDefaultLocale();
    sessionLocaleResolver.setLocale(request, response, locale);
  }

  private static boolean isValidLang(String lang) {
    for (Lang existingLang : Lang.values()) {
      if (existingLang.toString().equalsIgnoreCase(lang)) {
        return true;
      }
    }
    return false;
  }

}
