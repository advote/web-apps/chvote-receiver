# CHVote Vote Receiver - Backend

This is the root of the Maven multi-modules project that build the backend of the Vote Receiver.

# Modules

| Sub-module name | description |
| --- | --- |
| vr-backend-fixtures | Contains utilities to create sets of testing data, used by unit tests and by the mock-server |
| vr-backend-mock-server | Mock Server, providing endpoints to create sets of data for testing, endpoints to simulate the PACT, VRUM and Protocol interfaces |
| vr-backend-repository | JPA entities and repositories |
| vr-backend-rest | Spring boot web application exposing REST endpoints for the frontend application, and a web servlet to serve the "list of canton" page and "list of operations" page. | 
| vr-backend-service | Contains business services and logic |

# Development guidelines

- [Coding style for FrontEnd development](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-Java.md)
- [Code review checklist](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/code-review-checklist.md)
- [Unit testing principles](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/unit-testing-principles.md)

If you use IntelliJ IDEA, you should apply the [CHVote code style](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/CHvote-code-style.xml)

# Building

## Pre-requisites

* JDK 11 
* Maven 3.x
* Docker 

## Build command

```bash
cd backend
mvn clean install
```

# Running

## Run the application

The application entry-point is the module `vr-backend-rest`.
This module contains REST controllers exposed to the front-end Angular application. 

In order to have the backend application up and running, the following steps must be executed in this order:

### 1. RabbitMQ

Run a Rabbit-MQ queue where messages to the Protocol will be emitted:

```bash
docker-compose -f backend/vr-backend-rest/docker-compose.yml up
```

### 2. REST API

Run the REST API:

```bash
cd vr-backend-rest
SPRING_PROFILES_ACTIVE=dev,mocked-protocol mvn spring-boot:run
```

The profile ```mocked-protocol``` is used to start vote receiver backend application in a standalone mode 
i.e. without starting protocol instance.

### 3. Database

The backend application uses an in memory H2 database.

### 4. Mock server
Run the mock server that allows you to create mock data on demand and run the e2e tests:

```bash
cd vr-backend-mock-server/vr-backend-mock-server-rest
SPRING_PROFILES_ACTIVE=dev,mocked-protocol mvn spring-boot:run
```
  
The Mock server can be accessed through the following swagger interface: 
[http://localhost:57646/mock/swagger-ui.html](http://localhost:57646/mock/swagger-ui.html)
