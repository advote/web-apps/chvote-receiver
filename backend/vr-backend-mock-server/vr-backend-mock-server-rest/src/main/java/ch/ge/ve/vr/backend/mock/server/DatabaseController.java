/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.chvote.pact.b2b.client.model.OperationType;
import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.chvote.pact.b2b.client.model.VotingPeriod;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.vr.backend.fixtures.FileReference;
import ch.ge.ve.vr.backend.fixtures.OperationInstance;
import ch.ge.ve.vr.backend.fixtures.TestCases;
import ch.ge.ve.vr.backend.repository.OperationRepository;
import ch.ge.ve.vr.backend.repository.VoterRepository;
import ch.ge.ve.vr.backend.repository.data.Election;
import ch.ge.ve.vr.backend.repository.data.Operation;
import ch.ge.ve.vr.backend.repository.data.Votation;
import ch.ge.ve.vr.backend.repository.entity.Data;
import ch.ge.ve.vr.backend.repository.entity.ElectionHolder;
import ch.ge.ve.vr.backend.repository.entity.OperationHolder;
import ch.ge.ve.vr.backend.repository.entity.RawFile;
import ch.ge.ve.vr.backend.repository.entity.VotationHolder;
import ch.ge.ve.vr.backend.repository.entity.VoterHolder;
import ch.ge.ve.vr.backend.service.exception.TechnicalException;
import ch.ge.ve.vr.backend.service.operation.BirthDateScopeMapper;
import ch.ge.ve.vr.backend.service.voter.VoterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Transactional
public class DatabaseController {

  private static final Logger logger = LoggerFactory.getLogger(DatabaseController.class);

  public enum CantonForTest {
    GE, AG, BE, BS, LU, SG
  }

  public enum DateType {
    NOW, FUTURE, PAST
  }


  private final ObjectMapper             objectMapper;
  private final OperationRepository      operationRepository;
  private final VoterRepository          voterRepository;
  private final VrumSimulatorService     vrumSimulatorService;
  private final ProtocolSimulatorService protocolSimulatorService;
  private final VoterService             voterService;

  @Autowired
  public DatabaseController(ObjectMapper objectMapper,
                            OperationRepository operationRepository,
                            VrumSimulatorService vrumSimulatorService,
                            VoterRepository voterRepository,
                            ProtocolSimulatorService protocolSimulatorService,
                            VoterService voterService) {
    this.objectMapper = objectMapper;
    this.operationRepository = operationRepository;
    this.vrumSimulatorService = vrumSimulatorService;
    this.voterRepository = voterRepository;
    this.protocolSimulatorService = protocolSimulatorService;
    this.voterService = voterService;
  }

  /**
   * Reset database to create a new test
   */
  @GetMapping(path = "database/reset")
  public void reset() {
    logger.info("Start cleaning database ...");
    protocolSimulatorService.clearAll();
    voterRepository.deleteAll();
    operationRepository.deleteAll();
    vrumSimulatorService.clear();
    logger.info("The database has been cleaned !");
  }


  @GetMapping(path = "create-use-case")
  public CreatedOperation createOperationInstance(
      @RequestParam(value = "type") OperationType type,
      @RequestParam(value = "canton") CantonForTest canton,
      @RequestParam(value = "withElection") boolean withElection,
      @RequestParam(value = "withVotation") boolean withVotation,
      @RequestParam(value = "groupedVotation") boolean groupedVotation,
      @RequestParam(value = "dateType", defaultValue = "NOW") DateType dateType)
      throws NotEnoughPrimesInGroupException {

    final VotingPeriod votingPeriod = createVotingPeriod(dateType);
    var operationInstance = TestCases
        .createSimpleOperation(type, canton.name(), withVotation, withElection, groupedVotation, votingPeriod,
                               votingPeriod.getClosingDate().plusDays(1));

    var protocolId = protocolSimulatorService.initProtocol(operationInstance.getVotations());

    operationInstance = addProtocolInfo(operationInstance, protocolId);

    // save operation in database
    saveOperation(operationInstance, protocolId);

    return new CreatedOperation(operationInstance.getOperation().getProtocolInstanceId(),
                                protocolSimulatorService.getVotingCards(protocolId));
  }


  private OperationInstance addProtocolInfo(OperationInstance operationInstance, String protocolId) {
    operationInstance = new OperationInstance(
        new Operation(
            operationInstance.getOperation().getCanton(),
            protocolId,
            operationInstance.getOperation().getType(),
            operationInstance.getOperation().getVotingCardLabel(),
            operationInstance.getOperation().getSimulationName(),
            operationInstance.getOperation().getDefaultLang(),
            operationInstance.getOperation().isGroupVotation(),
            operationInstance.getOperation().getTranslationFileIndex(),
            operationInstance.getOperation().getVotingPeriod(),
            operationInstance.getOperation().getDate(),
            operationInstance.getOperation().getDocumentations(),
            operationInstance.getOperation().getHighlightedQuestions()
        ),
        operationInstance.getVotations(),
        operationInstance.getElections(),
        operationInstance.getRawFiles()
    );
    return operationInstance;
  }

  private VotingPeriod createVotingPeriod(DateType dateType) {
    switch (dateType) {
      case FUTURE:
        return new VotingPeriod(LocalDateTime.of(2045, 10, 10, 11, 5),
                                LocalDateTime.of(2046, 11, 11, 12, 5), 9);
      case PAST:
        return new VotingPeriod(LocalDateTime.of(2005, 8, 8, 2, 5),
                                LocalDateTime.of(2006, 8, 8, 3, 5), 9);
    }
    return new VotingPeriod(LocalDateTime.now(), LocalDateTime.now().plusDays(28), 9);
  }

  public static class CreatedOperation {
    final String           protocolInstanceId;
    final List<VotingCard> votingCards;

    CreatedOperation(String protocolInstanceId, List<VotingCard> votingCards) {
      this.protocolInstanceId = protocolInstanceId;
      this.votingCards = votingCards;
    }

    public String getProtocolInstanceId() {
      return protocolInstanceId;
    }

    public List<VotingCard> getVotingCards() {
      return votingCards;
    }
  }

  private void saveOperation(OperationInstance operationInstance, String protocolInstanceId) {

    // map to an OperationHolder
    OperationHolder operationHolder = new OperationHolder();
    operationHolder.setModelVersion(1);
    operationHolder.setCanton(operationInstance.getOperation().getCanton());
    operationHolder.setLabel(operationInstance.getOperation().getVotingCardLabel());
    operationHolder.setDefaultLang(operationInstance.getOperation().getDefaultLang());
    operationHolder.setClosingDate(operationInstance.getOperation().getVotingPeriod().getClosingDate());
    operationHolder.setOpeningDate(operationInstance.getOperation().getVotingPeriod().getOpeningDate());
    operationHolder.setGracePeriod(operationInstance.getOperation().getVotingPeriod().getGracePeriod());
    operationHolder.setDate(operationInstance.getOperation().getDate());
    operationHolder.setProtocolInstanceId(protocolInstanceId);
    operationHolder.setType(operationInstance.getOperation().getType());
    operationHolder.setTranslationFileIndex(operationInstance.getOperation().getTranslationFileIndex());

    // create raw files and corresponding documentation
    Set<RawFile> rawFiles = new HashSet<>();
    for (FileReference fileReference : operationInstance.getRawFiles()) {
      RawFile rawFile = new RawFile();
      rawFile.setIndex(fileReference.getIndex());
      rawFile.setName(fileReference.getFileName());
      rawFile.setContent(fileReference.getContent());
      rawFile.setOperation(operationHolder);
      rawFiles.add(rawFile);
    }
    operationHolder.setRawFiles(rawFiles);

    // votation ballots
    addVotations(operationInstance.getVotations(), operationHolder);

    // election ballots
    addElections(operationInstance.getElections(), operationHolder);

    attachData(operationHolder, operationInstance.getOperation());

    operationRepository.save(operationHolder);

    // import voters only if this is a new operation
    voterRepository.saveAll(createVoters(protocolSimulatorService.getVoters(protocolInstanceId), protocolInstanceId));
  }

  private <T> void attachData(Data<T> entity, T data) {
    try {
      entity.setData(objectMapper.writeValueAsString(data));
    } catch (JsonProcessingException e) {
      throw new TechnicalException(e);
    }
  }

  private void addVotations(List<Votation> votations, OperationHolder operationHolder) {
    List<VotationHolder> votationHolders = new ArrayList<>();

    for (Votation votation : votations) {

      VotationHolder votationHolder = new VotationHolder();
      votationHolder.setOperation(operationHolder);
      attachData(votationHolder, votation);
      votationHolders.add(votationHolder);
    }

    operationHolder.setVotations(votationHolders);
  }

  private void addElections(List<Election> elections, OperationHolder operationHolder) {
    List<ElectionHolder> electionHolders = new ArrayList<>();

    for (Election election : elections) {

      ElectionHolder electionHolder = new ElectionHolder();
      electionHolder.setOperation(operationHolder);
      attachData(electionHolder, election);
      electionHolders.add(electionHolder);
    }

    operationHolder.setElections(electionHolders);
  }

  private Set<VoterHolder> createVoters(Set<Voter> voters, String protocolInstanceId) {
    return voters.stream().map(voter -> {
      VoterHolder voterHolder = new VoterHolder();
      voterHolder.setProtocolInstanceId(protocolInstanceId);
      voterHolder.setDomainOfInfluences(voter.getDomainOfInfluenceIds()
                                             .stream()
                                             .map(DomainOfInfluence::getIdentifier)
                                             .collect(Collectors.toSet()));
      voterHolder.setVoterId(voter.getVoterIndex());
      voterHolder.setVoterBirthDateScope(BirthDateScopeMapper.mapToScope(voter.getVoterBirthDay(), voter.getVoterBirthMonth()));
      voterHolder.setVoterBirthHashed(
          voterService.hashDateOfBirth(
              voter.getVoterBirthDay(), voter.getVoterBirthMonth(), voter.getVoterBirthYear()));
      return voterHolder;
    }).collect(Collectors.toSet());
  }
}
