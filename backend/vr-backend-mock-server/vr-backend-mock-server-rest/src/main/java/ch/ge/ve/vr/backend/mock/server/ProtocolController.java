/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import java.math.BigInteger;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/protocol")
public class ProtocolController {

  private final ProtocolSimulatorService protocolSimulatorService;

  public ProtocolController(ProtocolSimulatorService protocolSimulatorService) {
    this.protocolSimulatorService = protocolSimulatorService;
  }

  @GetMapping(value = "/public-parameters")
  public PublicParameters getPublicParameters() {
    return protocolSimulatorService.getPublicParameters();
  }

  @GetMapping(value = "/primes/{protocolId}")
  public List<BigInteger> getPrimes(@PathVariable("protocolId") String protocolId) {
    return protocolSimulatorService.getPrimes(protocolId);
  }

  @GetMapping(value = "/public-keys/{protocolId}")
  public List<EncryptionPublicKey> getPublicKeys(@PathVariable("protocolId") String protocolId) {
    return protocolSimulatorService.getPublicKeys(protocolId);
  }

  @GetMapping(value = "/election-set/{protocolId}")
  public ElectionSetWithPublicKey getElectionSet(@PathVariable("protocolId") String protocolId) {
    return protocolSimulatorService.getElectionSet(protocolId);
  }

  @GetMapping(value = "/officer-public-key")
  public EncryptionPublicKey getOfficerPublicKey() {
    return protocolSimulatorService.getOfficerPublicKey();
  }

  @PostMapping(value = "/submit-ballot/{protocolId}/{voterId}")
  public List<ObliviousTransferResponse> submitBallot(@RequestBody BallotAndQuery ballotAndQuery,
                                                      @PathVariable("protocolId") String protocolId,
                                                      @PathVariable("voterId") Integer voterId) {
    return protocolSimulatorService.submitBallot(ballotAndQuery, protocolId, voterId);
  }

  @PostMapping(value = "/submit-confirmation-code/{protocolId}/{voterId}")
  public List<FinalizationCodePart> submitConfirmationCode(@RequestBody Confirmation confirmation,
                                                           @PathVariable("protocolId") String protocolId,
                                                           @PathVariable("voterId") Integer voterId)
      throws ConfirmationFailedException {
    return protocolSimulatorService.submitConfirmationCode(confirmation, protocolId, voterId);
  }
}
