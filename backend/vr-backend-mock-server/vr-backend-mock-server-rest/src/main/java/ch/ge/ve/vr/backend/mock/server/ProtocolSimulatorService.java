/**************************************************************************************************
 *                                                                                                *
 *  - #%L                                                                                         *
 *  - CHVote-Receiver                                                                             *
 *  - %%                                                                                          *
 *  - Copyright (C) 2016 - 2018 République et Canton de Genève                                    *
 *  - %%                                                                                          *
 *  - This program is free software: you can redistribute it and/or modify                        *
 *  - it under the terms of the GNU Affero General Public License as published by                 *
 *  - the Free Software Foundation, either version 3 of the License, or                           *
 *  - (at your option) any later version.                                                         *
 *  -                                                                                             *
 *  - This program is distributed in the hope that it will be useful,                             *
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                              *
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                *
 *  - GNU General Public License for more details.                                                *
 *  -                                                                                             *
 *  - You should have received a copy of the GNU Affero General Public License                    *
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                        *
 *  - #L%                                                                                         *
 *                                                                                                *
 **************************************************************************************************/

package ch.ge.ve.vr.backend.mock.server;

import static com.google.common.collect.Lists.newArrayList;

import ch.ge.ve.chvote.pact.b2b.client.model.Voter;
import ch.ge.ve.config.VerificationKeysConfiguration;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.controlcomponent.DefaultControlComponent;
import ch.ge.ve.protocol.controlcomponent.exception.SignEncryptionException;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentCachedState;
import ch.ge.ve.protocol.controlcomponent.state.ControlComponentState;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.EncryptionKeyPair;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.VoterData;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.eventlog.state.CleanupCommand;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.simulation.eventlog.EventLogCommandInMemoryImpl;
import ch.ge.ve.protocol.simulation.eventlog.EventLogQueryInMemoryImpl;
import ch.ge.ve.protocol.simulation.exception.SimulationRuntimeException;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.vr.backend.fixtures.DoiIds;
import ch.ge.ve.vr.backend.repository.data.Subject;
import ch.ge.ve.vr.backend.repository.data.VariantSubjects;
import ch.ge.ve.vr.backend.repository.data.Votation;
import ch.ge.ve.vr.backend.service.model.BirthDateScope;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.springframework.stereotype.Service;

@Service
public class ProtocolSimulatorService {

  private static final int    SECURITY_LEVEL         = 1;
  private static final String ALGO_KEYSPEC_ALGORITHM = "AES";
  private static final String ALGO_CIPHER_PROVIDER   = "BC";
  private static final String ALGO_CIPHER_ALGORITHM  = "AES/CTR/NoPadding";
  private static final String ALGO_DIGEST_PROVIDER   = "BC";
  private static final String ALGO_DIGEST_ALGORITHM  = "BLAKE2B-256";
  private static final String ALGO_RNG_PROVIDER      = "SUN";
  private static final String ALGO_RNG_ALGORITHM     = "SHA1PRNG";

  private static final DomainOfInfluence DOMAIN_FED                          = new DomainOfInfluence("CH");
  private static final DomainOfInfluence DOMAIN_GE                           = new DomainOfInfluence("GE");
  private static final DomainOfInfluence DOMAIN_VGE                          = new DomainOfInfluence("6621");
  private static final String            CANDIDATE_DESCRIPTION_TEMPLATE      = "candidate %d";
  private static final String            FAKE_CANDIDATE_DESCRIPTION_TEMPLATE = "fake candidate %d";

  private final ObjectMapper                         objectMapper;
  private final AlgorithmsSpec                       algorithmsSpec;
  private final PublicParameters                     publicParameters;
  private final RandomGenerator                      randomGenerator;
  private final Map<String, Map<String, BigInteger>> verificationKeys;
  private final PrintingAuthorityWithPublicKey       printingAuthority;
  private final IdentificationPrivateKey             printingAuthorityDecryptionKey;

  private VotingCardPreparationAlgorithms votingCardPreparationAlgorithms;
  private ChannelSecurityAlgorithms       channelSecurityAlgorithms;
  private List<ControlComponent>          controlComponents;
  private List<BigInteger>                controlComponentsVerificationKeys = new ArrayList<>();
  private EncryptionKeyPair               electionOfficerKeyPair;

  private Map<String, ProtocolInstance> protocols = new HashMap<>();

  public ProtocolSimulatorService(ObjectMapper objectMapper) throws IOException, ChannelSecurityException {
    this.objectMapper = objectMapper;
    this.algorithmsSpec = new AlgorithmsSpec(
        ALGO_KEYSPEC_ALGORITHM,
        ALGO_CIPHER_PROVIDER,
        ALGO_CIPHER_ALGORITHM,
        ALGO_DIGEST_PROVIDER,
        ALGO_DIGEST_ALGORITHM,
        ALGO_RNG_PROVIDER,
        ALGO_RNG_ALGORITHM
    );
    this.publicParameters = PublicParametersFactory.forLevel(SECURITY_LEVEL).createPublicParameters(2);
    this.randomGenerator = RandomGenerator.create(algorithmsSpec.getRandomGeneratorAlgorithm(),
                                                  algorithmsSpec.getRandomGeneratorProvider());
    // load verification keys
    VerificationKeysConfiguration verificationKeysConfiguration = new VerificationKeysConfiguration();
    this.verificationKeys =
        verificationKeysConfiguration.verificationKeys(SECURITY_LEVEL, algorithmsSpec, objectMapper);

    // create printing authority
    printingAuthorityDecryptionKey = objectMapper.readValue(
        ProtocolSimulatorService.class.getResourceAsStream(String.format("/level%d/pa-e.sk", SECURITY_LEVEL)),
        IdentificationPrivateKey.class
    );

    TypeReference<Map<String, List<VerificationKey>>> typeRef = new TypeReference<>() {
    };

    Map<String, List<VerificationKey>> rawKeys = objectMapper.readValue(
        ProtocolSimulatorService.class.getResourceAsStream(String.format("/level%d/pa-e.pk", SECURITY_LEVEL)),
        typeRef
    );

    printingAuthority = new PrintingAuthorityWithPublicKey(
        "PrintingAuthority",
        rawKeys.get("PrintingAuthority").get(0).getPublicKey()
    );

    // create algorithms
    KeyEstablishmentAlgorithms keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator);
    votingCardPreparationAlgorithms = new VotingCardPreparationAlgorithms(publicParameters);

    Hash hash = new Hash(algorithmsSpec.getDigestAlgorithm(), algorithmsSpec.getDigestProvider(),
                         publicParameters.getSecurityParameters().getUpper_l());
    channelSecurityAlgorithms =
        new ChannelSecurityAlgorithms(algorithmsSpec, hash, publicParameters.getIdentificationGroup(), randomGenerator);

    // create election's officer key pair
    electionOfficerKeyPair = keyEstablishmentAlgorithms.generateKeyPair(publicParameters.getEncryptionGroup());

    // create control components
    createControlComponents();
  }

  void clearAll() {
    final Set<String> protocolIds = protocols.keySet();
    controlComponents.parallelStream().forEach(cc -> protocolIds.forEach(cc::cleanUp));
    protocols.clear();
  }

  String initProtocol(List<Votation> votations) throws NotEnoughPrimesInGroupException {
    String protocolId = UUID.randomUUID().toString();

    ProtocolInstance protocolInstance = new ProtocolInstance(protocolId);

    createElectionSetAndVoters(protocolInstance, votations);
    runInitialisation(protocolInstance);

    protocols.put(protocolId, protocolInstance);

    return protocolId;
  }

  Set<Voter> getVoters(String protocolId) {
    final ProtocolInstance protocolInstance = protocols.get(protocolId);
    return new HashSet<>(protocolInstance.getReceiverVoters().values());
  }

  List<VotingCard> getVotingCards(String protocolId) {
    final ProtocolInstance protocolInstance = protocols.get(protocolId);
    final List<ch.ge.ve.protocol.core.model.VotingCard> votingCards = buildVotingCards(protocolInstance);
    return votingCards
        .stream()
        .sorted(Comparator.comparingInt(v -> v.getVoter().getVoterId()))
        .map(v -> {
          Voter voter = protocolInstance.getReceiverVoters().get(v.getVoter().getVoterId());
          return new VotingCard(v.getVoter().getVoterId(),
                                v.getUpper_x(),
                                v.getUpper_y(),
                                v.getUpper_fc(),
                                v.getBold_rc(),
                                BirthDateScope.of(
                                    voter.getVoterBirthMonth() != null,
                                    voter.getVoterBirthDay() != null)
          );
        })
        .collect(Collectors.toList());
  }

  PublicParameters getPublicParameters() {
    return publicParameters;
  }

  List<BigInteger> getPrimes(String protocolId) {
    return protocols.get(protocolId).getPrimesCache().getPrimes();
  }

  List<EncryptionPublicKey> getPublicKeys(String protocolId) {
    return protocols.get(protocolId).getPublicKeys();
  }


  EncryptionPublicKey getOfficerPublicKey() {
    return this.electionOfficerKeyPair.getPublicKey();
  }

  List<ObliviousTransferResponse> submitBallot(BallotAndQuery ballotAndQuery, String protocolId, Integer voterId) {
    return controlComponents.parallelStream()
                            .map(authority -> authority.handleBallot(protocolId, voterId, ballotAndQuery))
                            .collect(Collectors.toList());
  }

  List<FinalizationCodePart> submitConfirmationCode(Confirmation confirmation, String protocolId, Integer voterId)
      throws ConfirmationFailedException {

    try {
      return controlComponents
          .parallelStream()
          .map(authority -> {
            if (authority.storeConfirmationIfValid(protocolId, voterId, confirmation)) {
              return authority.getFinalizationCodePart(protocolId, voterId);
            } else {
              throw new RuntimeExceptionWrapper(new ConfirmationFailedException());
            }
          })
          .collect(Collectors.toList());
    } catch (RuntimeExceptionWrapper e) {
      throw (ConfirmationFailedException) e.getCause();
    }
  }

  private void createControlComponents() {
    final JsonConverter jsonConverter = new JsonConverter(objectMapper);

    controlComponents = IntStream.range(0, publicParameters.getS()).mapToObj(ccIndex -> {
      try {
        final Map<String, BigInteger> ccKeys = verificationKeys.get(String.format("cc%d", ccIndex));

        IdentificationPrivateKey signingKey = objectMapper.readValue(
            ProtocolSimulatorService.class
                .getResourceAsStream(String.format("/level%d/cc%d.1-s.sk", SECURITY_LEVEL, ccIndex)),
            IdentificationPrivateKey.class
        );
        controlComponentsVerificationKeys.add(ccKeys.get(ccKeys.keySet().iterator().next()));

        final EventLogCommandInMemoryImpl ccEventLogCommand = new EventLogCommandInMemoryImpl();
        final EventLogQueryInMemoryImpl ccEventLogQuery = new EventLogQueryInMemoryImpl(ccEventLogCommand);

        final ControlComponentState state = new ControlComponentState(ccEventLogCommand, ccEventLogQuery, jsonConverter,
                                                                      new CleanupCommand(ccEventLogCommand));

        return new DefaultControlComponent(
            ccIndex,
            25,
            algorithmsSpec,
            objectMapper,
            randomGenerator,
            state,
            new ControlComponentCachedState(state),
            signingKey,
            () -> {
              // do nothing
            });

      } catch (IOException e) {
        throw new RuntimeException(String.format("Failed to load the control components keys (%d)", ccIndex), e);
      }
    }).collect(Collectors.toList());
  }

  private void runInitialisation(ProtocolInstance protocolInstance) {
    // publish public parameters and election set to control components
    controlComponents.parallelStream().forEach(cc -> {
      cc.storePublicParameters(protocolInstance.getProtocolId(), publicParameters);
      cc.storeElectionSet(protocolInstance.getProtocolId(), protocolInstance.getElectionSet());
    });

    runKeyGeneration(protocolInstance);
    runCredentialsGeneration(protocolInstance);
  }

  private void runKeyGeneration(ProtocolInstance protocolInstance) {
    controlComponents.forEach(cc -> {
      EncryptionPublicKey pk = cc.generateKeys(protocolInstance.getProtocolId());
      protocolInstance.getPublicKeys().add(pk);
      controlComponents.parallelStream().forEach(
          controlComponent -> controlComponent
              .storeKeyPart(protocolInstance.getProtocolId(), cc.getIndex(), pk));
    });

    controlComponents.parallelStream()
                     .forEach(
                         cc -> cc.storeElectionOfficerPublicKey(protocolInstance.getProtocolId(),
                                                                electionOfficerKeyPair.getPublicKey()));
  }

  private void runCredentialsGeneration(ProtocolInstance protocolInstance) {
    for (int i = 0; i < controlComponents.size(); i++) {
      int ccIndex = i;
      ControlComponent controlComponent = controlComponents.get(i);
      List<VoterData> electorateData =
          controlComponent.generateVoterData(protocolInstance.getProtocolId(), protocolInstance.getProtocolVoters());
      Map<Integer, Point> points = electorateData.stream()
                                                 .collect(Collectors.toMap(
                                                     VoterData::getVoterId,
                                                     VoterData::getPublicVoterData
                                                 ));
      controlComponents.parallelStream()
                       .forEach(cc -> cc.storePublicCredentialsPart(protocolInstance.getProtocolId(), ccIndex, points));
      try {
        controlComponent.generatePrimes(protocolInstance.getProtocolId());
      } catch (NotEnoughPrimesInGroupException e) {
        throw new RuntimeException("Failed to generate the primes", e);
      }
    }
    controlComponents.parallelStream().forEach(
        cc -> cc.buildPublicCredentials(protocolInstance.getProtocolId(),
                                        protocolInstance.getProtocolVoters().stream()
                                                        .map(ch.ge.ve.protocol.model.Voter::getVoterId)
                                                        .collect(Collectors.toList()))
    );
  }

  private List<ch.ge.ve.protocol.core.model.VotingCard> buildVotingCards(ProtocolInstance protocolInstance) {
    List<Map<PrintingAuthorityWithPublicKey, byte[]>> controlComponentsCredentials =
        controlComponents.stream().map(c -> {
          try {
            return c.getSignEncryptedPrivateCredentials(protocolInstance.getProtocolId(),
                                                        protocolInstance.getProtocolVoters().stream()
                                                                        .map(ch.ge.ve.protocol.model.Voter::getVoterId)
                                                                        .collect(Collectors.toList()));
          } catch (SignEncryptionException e) {
            throw new RuntimeException("Failed to retrieved the signEncrypted credentials", e);
          }
        }).collect(Collectors.toList());

    List<byte[]> printingAuthorityVoterCredentials =
        controlComponentsCredentials.stream().map(
            m -> m.computeIfAbsent(protocolInstance.getElectionSet().getPrintingAuthorities().get(0), k -> new byte[0]))
                                    .collect(Collectors.toList());

    List<List<SecretVoterData>> voterDataMatrix =
        IntStream.range(0, printingAuthorityVoterCredentials.size()).mapToObj(ccIndex -> {
          try {
            byte[] signEncryptedCreds = printingAuthorityVoterCredentials.get(ccIndex);
            BigInteger signingPublicKey = controlComponentsVerificationKeys.get(ccIndex);

            String serializedCreds = channelSecurityAlgorithms
                .decryptAndVerify(printingAuthorityDecryptionKey.getPrivateKey(), signingPublicKey,
                                  deserializeEncryptedMessage(signEncryptedCreds));

            return deserializeSecretVoterDataList(serializedCreds);
          } catch (ChannelSecurityException e) {
            throw new SimulationRuntimeException("Failed to decrypt / verify the signEncrypted credentials", e);
          }
        }).collect(Collectors.toList());

    return votingCardPreparationAlgorithms.getVotingCards(protocolInstance.getElectionSet(), voterDataMatrix);
  }


  private EncryptedMessageWithSignature deserializeEncryptedMessage(byte[] cipher) {
    try {
      return objectMapper.readValue(cipher, EncryptedMessageWithSignature.class);
    } catch (IOException e) {
      throw new RuntimeException("Failed to deserialize the cipher with signature", e);
    }
  }

  private List<SecretVoterData> deserializeSecretVoterDataList(String serializedCreds) {
    try {
      return objectMapper.readValue(serializedCreds, new TypeReference<List<SecretVoterData>>() {
      });
    } catch (IOException e) {
      throw new RuntimeException("Failed to deserialize the secret voter data list", e);
    }
  }

  private void createElectionSetAndVoters(ProtocolInstance protocolInstance, List<Votation> votations)
      throws NotEnoughPrimesInGroupException {

    createVoters(protocolInstance);

    int numberOfCandidates = 0;
    int numberOfFakeCandidates = 0;
    List<Election> elections = new ArrayList<>();

    int subjectId = 1;
    for (Votation votation : votations) {
      for (VariantSubjects variantSubjects : votation.getVariantSubjects()) {
        for (Subject subject : variantSubjects.getSubjects()) {
          elections.add(new Election(String.format("variant-subject-%d", subjectId++), 3, 1,
                                     mapToDomainOfInfluence(votation.getDomainOfInfluence())));
          numberOfCandidates += 2;
          numberOfFakeCandidates++;
        }
        // tie break
        elections.add(new Election(String.format("tie-break-%d", subjectId++), 3, 1,
                                   mapToDomainOfInfluence(votation.getDomainOfInfluence())));
        numberOfCandidates += 2;
        numberOfFakeCandidates++;
      }
      for (Subject subject : votation.getSubjects()) {
        elections.add(new Election(String.format("subject-%d", subjectId++), 3, 1,
                                   mapToDomainOfInfluence(votation.getDomainOfInfluence())));
        numberOfCandidates += 2;
        numberOfFakeCandidates++;
      }
    }

    List<Candidate> candidates = IntStream.range(0, numberOfCandidates)
                                          .mapToObj(
                                              i -> new Candidate(String.format(CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                          .collect(Collectors.toList());

    List<Candidate> fakeCandidates = IntStream.range(0, numberOfFakeCandidates)
                                              .mapToObj(i -> new Candidate(
                                                  String.format(FAKE_CANDIDATE_DESCRIPTION_TEMPLATE, i)))
                                              .collect(Collectors.toList());

    candidates.addAll(fakeCandidates);

    protocolInstance.setPrimesCache(
        PrimesCache.populate(numberOfCandidates + numberOfFakeCandidates, this.publicParameters.getEncryptionGroup()));

    protocolInstance.setElectionSet(
        new ElectionSetWithPublicKey(elections, candidates, newArrayList(printingAuthority),
                                     protocolInstance.getProtocolVoters().size(), 1));
  }

  private void createVoters(ProtocolInstance protocolInstance) {
    List<ch.ge.ve.protocol.model.Voter> protocolVoters = new ArrayList<>();
    Map<Integer, Voter> receiverVoters = new HashMap<>();

    CountingCircle countingCircle = new CountingCircle(1, "CountingCircle", "MyCountingCircle");

    String printingAuthorityName = printingAuthority.getName();
    protocolVoters.add(
        new ch.ge.ve.protocol.model.Voter(1, "1", countingCircle, printingAuthorityName, DOMAIN_FED, DOMAIN_GE,
                                          DOMAIN_VGE));
    protocolVoters
        .add(new ch.ge.ve.protocol.model.Voter(2, "2", countingCircle, printingAuthorityName, DOMAIN_FED, DOMAIN_GE));
    protocolVoters.add(new ch.ge.ve.protocol.model.Voter(3, "3", countingCircle, printingAuthorityName, DOMAIN_FED));
    protocolVoters.add(new ch.ge.ve.protocol.model.Voter(4, "4", countingCircle, printingAuthorityName, DOMAIN_VGE));
    protocolVoters.add(new ch.ge.ve.protocol.model.Voter(5, "5", countingCircle, printingAuthorityName));

    receiverVoters.put(1, voter(1, BirthDateScope.DAY, DoiIds.DOI_FED, DoiIds.DOI_GE, DoiIds.DOI_VGE));
    receiverVoters.put(2, voter(2, BirthDateScope.MONTH, DoiIds.DOI_FED, DoiIds.DOI_GE));
    receiverVoters.put(3, voter(3, BirthDateScope.YEAR, DoiIds.DOI_FED));
    receiverVoters.put(4, voter(4, BirthDateScope.DAY, DoiIds.DOI_VGE));
    receiverVoters.put(5, voter(5, BirthDateScope.DAY));

    protocolInstance.setProtocolVoters(protocolVoters);
    protocolInstance.setReceiverVoters(receiverVoters);
  }

  private Voter voter(int voterId, BirthDateScope birthDateScope, String... doiIds) {
    return new Voter(Stream.of(doiIds).map(DomainOfInfluence::new).collect(Collectors.toList()),
                     null,
                     voterId,
                     1980,
                     birthDateScope != BirthDateScope.YEAR ? 8 : null,
                     birthDateScope == BirthDateScope.DAY ? 25 : null,
                     null,
                     null);
  }

  private DomainOfInfluence mapToDomainOfInfluence(String doiId) {
    if (DoiIds.DOI_FED.equals(doiId)) {
      return DOMAIN_FED;
    }
    if (DoiIds.DOI_GE.equals(doiId)) {
      return DOMAIN_GE;
    }
    return DOMAIN_VGE;
  }

  ElectionSetWithPublicKey getElectionSet(String protocolId) {
    return protocols.get(protocolId).getElectionSet();
  }

  private static final class VerificationKey {

    private final String     keyId;
    private final BigInteger publicKey;

    @JsonCreator
    public VerificationKey(@JsonProperty("keyId") String keyId, @JsonProperty("publicKey") BigInteger publicKey) {
      this.keyId = keyId;
      this.publicKey = publicKey;
    }

    public String getKeyId() {
      return keyId;
    }

    public BigInteger getPublicKey() {
      return publicKey;
    }
  }
}
